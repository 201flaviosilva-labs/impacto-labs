class Square extends GameObject {
    constructor(context, x, y, vx, vy, mass) {
        super(context, x, y, vx, vy, mass);

        //Set default width and height
        this.width = this.randomInt(10, 100);
        this.height = this.randomInt(10, 100);
    }

    randomFloat(min, max, precision = 2) {
        if (!max) {
            max = min;
            min = 0;
        }
        return parseFloat((Math.random() * (max - min) + min).toFixed(precision));
    }

    randomInt(min, max) { return Math.floor(this.randomFloat(min, max)); };

    draw() {
        //Draw a simple square
        this.context.fillStyle = this.isColliding ? "#ff8080" : "#0099b0";
        this.context.fillRect(this.x, this.y, this.width, this.height);
    }

    update(secondsPassed) {
        //Move with set velocity
        this.x += this.vx * secondsPassed;
        this.y += this.vy * secondsPassed;
    }
}
