// https://stackoverflow.com/questions/45370692/circle-rectangle-collision-response
const ctx = canvas.getContext("2d");
const mouse = { x: 0, y: 0, button: false }
function mouseEvents(e) {
	mouse.x = e.pageX;
	mouse.y = e.pageY;
	mouse.button = e.type === "mousedown" ? true : e.type === "mouseup" ? false : mouse.button;
}
["down", "up", "move"].forEach(name => document.addEventListener("mouse" + name, mouseEvents));

// short cut vars 
var w = canvas.width;
var h = canvas.height;
var cw = w / 2;  // center 
var ch = h / 2;
const gravity = 1;


// constants and helpers
const PI2 = Math.PI * 2;
const setStyle = (ctx, style) => { Object.keys(style).forEach(key => ctx[key] = style[key]) };

// the ball
const ball = {
	r: 50,
	x: 50,
	y: 50,
	dx: 0.2,
	dy: 0.2,
	maxSpeed: 8,
	style: {
		lineWidth: 12,
		strokeStyle: "green",
	},
	draw(ctx) {
		setStyle(ctx, this.style);
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.r - this.style.lineWidth * 0.45, 0, PI2);
		ctx.stroke();
	},
	update() {
		this.dy += gravity;
		var speed = Math.sqrt(this.dx * this.dx + this.dy * this.dy);
		var x = this.x + this.dx;
		var y = this.y + this.dy;

		if (y > canvas.height - this.r) {
			y = (canvas.height - this.r) - (y - (canvas.height - this.r));
			this.dy = -this.dy;
		}
		if (y < this.r) {
			y = this.r - (y - this.r);
			this.dy = -this.dy;
		}
		if (x > canvas.width - this.r) {
			x = (canvas.width - this.r) - (x - (canvas.width - this.r));
			this.dx = -this.dx;
		}
		if (x < this.r) {
			x = this.r - (x - this.r);
			this.dx = -this.dx;
		}

		this.x = x;
		this.y = y;
		if (speed > this.maxSpeed) {  // if over speed then slow the ball down gradualy
			var reduceSpeed = this.maxSpeed + (speed - this.maxSpeed) * 0.9; // reduce speed if over max speed
			this.dx = (this.dx / speed) * reduceSpeed;
			this.dy = (this.dy / speed) * reduceSpeed;
		}


	}
}
const ballShadow = { // this is used to do calcs that may be dumped
	r: 50,
	x: 50,
	y: 50,
	dx: 0.2,
	dy: 0.2,
}
// Creates the bat
const bat = {
	x: 100,
	y: 250,
	dx: 0,
	dy: 0,
	width: 140,
	height: 10,
	style: {
		lineWidth: 2,
		strokeStyle: "black",
	},
	draw(ctx) {
		setStyle(ctx, this.style);
		ctx.strokeRect(this.x - this.width / 2, this.y - this.height / 2, this.width, this.height);
	},
	update() {
		this.dx = mouse.x - this.x;
		this.dy = mouse.y - this.y;
		var x = this.x + this.dx;
		var y = this.y + this.dy;
		x < this.width / 2 && (x = this.width / 2);
		y < this.height / 2 && (y = this.height / 2);
		x > canvas.width - this.width / 2 && (x = canvas.width - this.width / 2);
		y > canvas.height - this.height / 2 && (y = canvas.height - this.height / 2);
		this.dx = x - this.x;
		this.dy = y - this.y;
		this.x = x;
		this.y = y;

	}
}

//=============================================================================
// THE FUNCTION THAT DOES THE BALL BAT sim.
// the ball and bat are at new position
function doBatBall(bat, ball) {
	var mirrorX = 1;
	var mirrorY = 1;

	const s = ballShadow; // alias
	s.x = ball.x;
	s.y = ball.y;
	s.dx = ball.dx;
	s.dy = ball.dy;
	s.x -= s.dx;
	s.y -= s.dy;

	// get the bat half width height
	const batW2 = bat.width / 2;
	const batH2 = bat.height / 2;

	// and bat size plus radius of ball
	var batW = batW2 + ball.r;
	var batH = batH2 + ball.r;

	// set ball position relative to bats last pos
	s.x -= bat.x;
	s.y -= bat.y;

	// set ball delta relative to bat
	s.dx -= bat.dx;
	s.dy -= bat.dy;

	// mirror x and or y if needed
	if (s.x < 0) {
		mirrorX = -1;
		s.x = -s.x;
		s.dx = -s.dx;
	}
	if (s.y < 0) {
		mirrorY = -1;
		s.y = -s.y;
		s.dy = -s.dy;
	}


	// bat now only has a bottom, right sides and bottom right corner
	var distY = (batH - s.y); // distance from bottom 
	var distX = (batW - s.x); // distance from right

	if (s.dx > 0 && s.dy > 0) { return }// ball moving away so no hit

	var ballSpeed = Math.sqrt(s.dx * s.dx + s.dy * s.dy); // get ball speed relative to bat

	// get x location of intercept for bottom of bat
	var bottomX = s.x + (s.dx / s.dy) * distY;

	// get y location of intercept for right of bat
	var rightY = s.y + (s.dy / s.dx) * distX;

	// get distance to bottom and right intercepts
	var distB = Math.hypot(bottomX - s.x, batH - s.y);
	var distR = Math.hypot(batW - s.x, rightY - s.y);
	var hit = false;

	if (s.dy < 0 && bottomX <= batW2 && distB <= ballSpeed && distB < distR) {  // if hit is on bottom and bottom hit is closest
		hit = true;
		s.y = batH - s.dy * ((ballSpeed - distB) / ballSpeed);
		s.dy = -s.dy;
	}
	if (!hit && s.dx < 0 && rightY <= batH2 && distR <= ballSpeed && distR <= distB) { // if hit is on right and right hit is closest
		hit = true;
		s.x = batW - s.dx * ((ballSpeed - distR) / ballSpeed);;
		s.dx = -s.dx;
	}
	if (!hit) {  // if no hit may have intercepted the corner. 
		// find the distance that the corner is from the line segment from the balls pos to the next pos
		const u = ((batW2 - s.x) * s.dx + (batH2 - s.y) * s.dy) / (ballSpeed * ballSpeed);

		// get the closest point on the line to the corner
		var cpx = s.x + s.dx * u;
		var cpy = s.y + s.dy * u;

		// get ball radius squared
		const radSqr = ball.r * ball.r;

		// get the distance of that point from the corner squared
		const dist = (cpx - batW2) * (cpx - batW2) + (cpy - batH2) * (cpy - batH2);

		// is that distance greater than ball radius
		if (dist > radSqr) { return }  // no hit

		// solves the triangle from center to closest point on balls trajectory
		var d = Math.sqrt(radSqr - dist) / ballSpeed;

		// intercept point is closest to line start
		cpx -= s.dx * d;
		cpy -= s.dy * d;

		// get the distance from the ball current pos to the intercept point
		d = Math.hypot(cpx - s.x, cpy - s.y);

		// is the distance greater than the ball speed then its a miss
		if (d > ballSpeed) { return } // no hit return

		s.x = cpx;  // position of contact
		s.y = cpy;

		// find the normalised tangent at intercept point 
		const ty = (cpx - batW2) / ball.r;
		const tx = -(cpy - batH2) / ball.r;

		// calculate the reflection vector
		const bsx = s.dx / ballSpeed;   // normalise ball speed
		const bsy = s.dy / ballSpeed;
		const dot = (bsx * tx + bsy * ty) * 2;

		// get the distance the ball travels past the intercept
		d = ballSpeed - d;

		// the reflected vector is the balls new delta (this delta is normalised)
		s.dx = (tx * dot - bsx);
		s.dy = (ty * dot - bsy);

		// move the ball the remaining distance away from corner
		s.x += s.dx * d;
		s.y += s.dy * d;

		// set the ball delta to the balls speed
		s.dx *= ballSpeed;
		s.dy *= ballSpeed;
		hit = true;
	}

	// if the ball hit the bat restore absolute position
	if (hit) {
		// reverse mirror
		s.x *= mirrorX;
		s.dx *= mirrorX;
		s.y *= mirrorY;
		s.dy *= mirrorY;

		// remove bat relative position
		s.x += bat.x;
		s.y += bat.y;

		// remove bat relative delta
		s.dx += bat.dx;
		s.dy += bat.dy;

		// set the balls new position and delta
		ball.x = s.x;
		ball.y = s.y;
		ball.dx = s.dx;
		ball.dy = s.dy;
	}

}

// main update function
function update(timer) {

	if (w !== innerWidth || h !== innerHeight) {
		cw = (w = canvas.width = innerWidth) / 2;
		ch = (h = canvas.height = innerHeight) / 2;
	}



	ctx.setTransform(1, 0, 0, 1, 0, 0); // reset transform
	ctx.globalAlpha = 1;           // reset alpha
	ctx.clearRect(0, 0, w, h);

	// move bat and ball
	bat.update();
	ball.update();

	// check for bal bat contact and change ball position and trajectory if needed
	doBatBall(bat, ball);

	// draw ball and bat
	bat.draw(ctx);
	ball.draw(ctx);

	requestAnimationFrame(update);

}
requestAnimationFrame(update);
