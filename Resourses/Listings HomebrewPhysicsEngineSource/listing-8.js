resolveElastic: function(player, entity) {
    // https://developer.ibm.com/tutorials/wa-build2dphysicsengine/
    const dynamicReact1 = player;
    const dynamicReact2 = entity;

    // Find the mid points of the entity and player
    const pMidX = dynamicReact1.getCenter().x;
    const pMidY = dynamicReact1.getCenter().y;
    const aMidX = dynamicReact2.getCenter().x;
    const aMidY = dynamicReact2.getCenter().y;
    const STICKY_THRESHOLD = 0.0004;
    dynamicReact2.restitution = 1;

    // To find the side of entry calculate based on
    // the normalized sides
    const dx = (aMidX - pMidX) / (dynamicReact2.width / 2);
    const dy = (aMidY - pMidY) / (dynamicReact2.height / 2);

    // Calculate the absolute change in x and y
    const absDX = Math.abs(dx);
    const absDY = Math.abs(dy);

    // If the distance between the normalized x and y
    // position is less than a small threshold (.1 in this case)
    // then this object is approaching from a corner
    if (Math.abs(absDX - absDY) < .1) {

        // If the player is approaching from positive X
        if (dx < 0) {

            // Set the player x to the right side
            dynamicReact1.x = dynamicReact2.getRight();

            // If the player is approaching from negative X
        } else {

            // Set the player x to the left side
            dynamicReact1.x = dynamicReact2.getLeft() - dynamicReact1.width;
        }

        // If the player is approaching from positive Y
        if (dy < 0) {

            // Set the player y to the bottom
            dynamicReact1.y = dynamicReact2.getBottom();

            // If the player is approaching from negative Y
        } else {

            // Set the player y to the top
            dynamicReact1.y = dynamicReact2.getTop() - dynamicReact1.height;
        }

        // Randomly select a x/y direction to reflect velocity on
        if (Math.random() < .5) {

            // Reflect the velocity at a reduced rate
            dynamicReact1.velocity.x = -dynamicReact1.velocity.x * dynamicReact2.restitution;

            // If the object’s velocity is nearing 0, set it to 0
            // STICKY_THRESHOLD is set to .0004
            if (Math.abs(dynamicReact1.velocity.x) < STICKY_THRESHOLD) {
                dynamicReact1.velocity.x = 0;
            }
        } else {

            dynamicReact1.velocity.y = -dynamicReact1.velocity.y * dynamicReact2.restitution;
            if (Math.abs(dynamicReact1.velocity.y) < STICKY_THRESHOLD) {
                dynamicReact1.velocity.y = 0;
            }
        }

        // If the object is approaching from the sides
    } else if (absDX > absDY) {

        // If the player is approaching from positive X
        if (dx < 0) {
            dynamicReact1.x = dynamicReact2.getRight();

        } else {
            // If the player is approaching from negative X
            dynamicReact1.x = dynamicReact2.getLeft() - dynamicReact1.width;
        }

        // Velocity component
        dynamicReact1.velocity.x = -dynamicReact1.velocity.x * dynamicReact2.restitution;

        if (Math.abs(dynamicReact1.velocity.x) < STICKY_THRESHOLD) {
            dynamicReact1.velocity.x = 0;
        }

        // If this collision is coming from the top or bottom more
    } else {

        // If the player is approaching from positive Y
        if (dy < 0) {
            dynamicReact1.y = dynamicReact2.getBottom();

        } else {
            // If the player is approaching from negative Y
            dynamicReact1.y = dynamicReact2.getTop() - dynamicReact1.height;
        }

        // Velocity component
        dynamicReact1.velocity.y = -dynamicReact1.velocity.y * dynamicReact2.restitution;
        if (Math.abs(dynamicReact1.velocity.y) < STICKY_THRESHOLD) {
            dynamicReact1.velocity.y = 0;
        }
    }
};
