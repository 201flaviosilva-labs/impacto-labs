// https://stackoverflow.com/questions/38648693/resolve-collision-of-two-2d-elements
// Collision boolean (useful for passive collision detection)
function collision(A, B) {
	return !(((A.y + A.h) < (B.y)) || (A.y > (B.y + B.h)) || ((A.x + A.w) < B.x) || (A.x > (B.x + B.w)));
}

// Resolve collision
function resolveCollision(A, B) {
	// get the vectors to check against
	var vX = (A.x + (A.w / 2)) - (B.x + (B.w / 2)),
		vY = (A.y + (A.h / 2)) - (B.y + (B.h / 2)),
		// Half widths and half heights of the objects
		ww2 = (A.w / 2) + (B.w / 2),
		hh2 = (A.h / 2) + (B.h / 2),
		colDir = "";

	// if the x and y vector are less than the half width or half height,
	// they we must be inside the object, causing a collision
	if (Math.abs(vX) < ww2 && Math.abs(vY) < hh2) {
		// figures out on which side we are colliding (top, bottom, left, or right)
		var oX = ww2 - Math.abs(vX),
			oY = hh2 - Math.abs(vY);
		if (oX >= oY) {
			if (vY > 0) {
				colDir = "TOP";
				A.y += oY;
			} else {
				colDir = "BOTTOM";
				A.y -= oY;
			}
		} else {
			if (vX > 0) {
				colDir = "LEFT";
				A.x += oX;
			} else {
				colDir = "RIGHT";
				A.x -= oX;
			}
		}
	}
	return colDir;
}


// Elements
var cvs = document.createElement("canvas"),
	ctx = cvs.getContext("2d"),
	EL_collisionInfo = document.getElementById("collisionInfo");


// Game variables
var Stage = {
	w: 300,
	h: 200
},
	Keys = {},
	Player = {
		x: 0,
		y: 0,
		w: 30,
		h: 30,
		color: "blue",
		velocity: 4,
		move: function () {
			if (Keys[65]) { // A
				this.x -= this.velocity;
			}
			if (Keys[87]) { // W
				this.y -= this.velocity;
			}
			if (Keys[68]) { // D
				this.x += this.velocity;
			}
			if (Keys[83]) { // S
				this.y += this.velocity;
			}

			// General Collision / Touching
			var orgColor = "";
			if (collision(this, Enemy)) {
				this.color = "red";
			} else { // not colliding
				this.color = "blue";
			}

			// Resolve collision
			var coll = resolveCollision(this, Enemy);
			// And write info on screen
			EL_collisionInfo.innerHTML = coll;

			// Prevent go out of Stage
			this.x = Math.min(Math.max(0, this.x), Stage.w - this.w);
			this.y = Math.min(Math.max(0, this.y), Stage.h - this.h);
		}
	},
	Enemy = {
		x: 130,
		y: 80,
		w: 50,
		h: 50,
		color: "red"
	};


// INIT canvas and size
document.body.appendChild(cvs);
cvs.width = Stage.w;
cvs.height = Stage.h;


function canvasDraw(el) {
	ctx.beginPath();
	ctx.fillStyle = el.color;
	ctx.fillRect(el.x, el.y, el.w, el.h);
}

// /////
// KEYBOARD LISTENERS
document.addEventListener("keydown", function (e) {
	Keys[e.which] = 1;
}, false);
document.addEventListener("keyup", function (e) {
	delete Keys[e.which];
}, false);


// /////
// ENGINE
(function engine() {

	Player.move();
	// Clear canvas and draw
	ctx.clearRect(0, 0, cvs.width, cvs.height);
	canvasDraw(Player);
	canvasDraw(Enemy);

	window.requestAnimationFrame(engine);
}());
