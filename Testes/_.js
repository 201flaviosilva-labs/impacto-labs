// -------- Antiga Versão
checkWillTopOverlap(other) { return other.getBottom() >= this.getNextBoxBoundTop() && other.getTop() <= this.getNextBoxBoundTop(); }
checkWillBottomOverlap(other) { return other.getTop() <= this.getNextBoxBoundBottom() && other.getBottom() >= this.getNextBoxBoundBottom(); }
checkWillLeftOverlap(other) { return other.getRight() >= this.getNextBoxBoundLeft() && other.getLeft() <= this.getNextBoxBoundLeft(); }
checkWillRightOverlap(other) { return other.getLeft() <= this.getNextBoxBoundRight() && other.getRight() >= this.getNextBoxBoundRight(); }
checkWillOverlap(other) {
	return other.getLeft() <= this.getNextBoxBoundRight() &&
		other.getRight() >= this.getNextBoxBoundLeft() &&
		other.getTop() <= this.getNextBoxBoundBottom() &&
		other.getBottom() >= this.getNextBoxBoundTop();
}

checkWillCollideTopWith(other) {
	if (this.checkWillLeftOverlap(other) || this.checkWillRightOverlap(other)) {
		return other.getBottom() >= this.getNextBoxBoundTop() && other.getTop() <= this.getNextBoxBoundBottom();
	}
	return false;
}

checkWillCollideBottomWith(other) {
	if (this.checkWillLeftOverlap(other) || this.checkWillRightOverlap(other)) {
		return other.getTop() <= this.getNextBoxBoundBottom() && other.getBottom() >= this.getNextBoxBoundTop();
	}
	return false;
}

checkWillCollideLeftWith(other) {
	if (this.checkWillTopOverlap(other) || this.checkWillBottomOverlap(other)) {
		return other.getRight() >= this.getNextBoxBoundLeft() && other.getLeft() <= this.getNextBoxBoundRight();
	}
	return false;
}

checkWillCollideRightWith(other) {
	if (this.checkWillTopOverlap(other) || this.checkWillBottomOverlap(other)) {
		return other.getLeft() <= this.getNextBoxBoundRight() && other.getRight() >= this.getNextBoxBoundLeft();
	}
	return false;
}

checkWillCollideVerticalWith(other) {
	return this.checkWillCollideTopWith(other) || this.checkWillCollideBottomWith(other);
}

checkWillCollideHorizontalWith(other) {
	return this.checkWillCollideLeftWith(other) || this.checkWillCollideRightWith(other);
}

checkWillCollideWith(other) {
	return this.checkWillCollideTopWith(other) ||
		this.checkWillCollideBottomWith(other) ||
		this.checkWillCollideLeftWith(other) ||
		this.checkWillCollideRightWith(other);
}

checkWillOverlap(other) {
	return other.getLeft() <= this.getNextBoxBoundRight() &&
		other.getRight() >= this.getNextBoxBoundLeft() &&
		other.getTop() <= this.getNextBoxBoundBottom() &&
		other.getBottom() >= this.getNextBoxBoundTop();
}

checkWillCollideTopWith(other) {
	return this.checkWillOverlap(other) && other.getBottom() >= this.getNextBoxBoundTop() && other.getTop() <= this.getNextBoxBoundBottom()
}

checkWillCollideBottomWith(other) {
	return this.checkWillOverlap(other) && other.getTop() <= this.getNextBoxBoundBottom() && other.getBottom() >= this.getNextBoxBoundTop();
}

checkWillCollideLeftWith(other) {
	return this.checkWillOverlap(other) && other.getRight() >= this.getNextBoxBoundLeft() && other.getLeft() <= this.getNextBoxBoundRight();
}

checkWillCollideRightWith(other) {
	return this.checkWillOverlap(other) && other.getLeft() <= this.getNextBoxBoundRight() && other.getRight() >= this.getNextBoxBoundLeft();
}


// ------- Outro Novo
{
	const aVector = new Vector2(this.getCenterX(), this.getCenterY());
	const bVector = new Vector2(other.getCenterX(), other.getCenterY());
	const distance = aVector.distance(bVector);
	const minDistance = this.width / 2 + other.width / 2;
	return distance <= minDistance;
}

// ------- Outro Novo Collisão layers
// Core function
collisionLayer(layer, currentScene) {
	layer.forEach((gameObject1, index1) => {
		layer.forEach((gameObject2, index2) => {
			if (index1 >= index2) return;
			// console.log(gameObject1.checkWillCollideWith(gameObject2));
			// console.log(gameObject1.width, gameObject2.width);

			// if (this.overlapRectangleAndRectangle(gameObject1, gameObject2)) {
			// 	const collisionBoundsRect = this.getBoundsOfTwoOverlappingRectangles(gameObject1, gameObject2);
			// 	const collisionRect = new Rectangle(
			// 		collisionBoundsRect.x,
			// 		collisionBoundsRect.y,
			// 		collisionBoundsRect.width,
			// 		collisionBoundsRect.height,
			// 		"#000000");
			// 	currentScene.addChild(collisionRect);
			// 	// gameObject1.setVelocity(0);
			// 	// gameObject2.setVelocity(0);
			// }
			// // return;

			const collision = this.detectRectangleAndRectangleCollisionDirections(gameObject1, gameObject2);
			if (collision.length < 0 || !collision) return;

			collision.forEach(collisionType => {
				const lastGameObject1XVelocity = gameObject1.velocity.x;
				const lastGameObject1YVelocity = gameObject1.velocity.y;
				const lastGameObject2XVelocity = gameObject2.velocity.x;
				const lastGameObject2YVelocity = gameObject2.velocity.y;
				const gravityX = GlobalStateManagerInstance.gravity.x;
				const gravityY = GlobalStateManagerInstance.gravity.y;

				// Vertical
				if (collisionType === "TOP" || collisionType === "BOTTOM") {
					// console.log("TOP/BOTTOM");
					if (gameObject1.bodyType === "D" && gameObject2.bodyType === "D") { // Dynamic X Dynamic
						gameObject1.setVelocityY(
							lastGameObject2YVelocity * gameObject1.bounce.y - gravityY
						);
						gameObject2.setVelocityY(
							lastGameObject1YVelocity * gameObject2.bounce.y - gravityY
						);

					} else if (gameObject1.bodyType !== "D" && gameObject2.bodyType === "D") { // (Kinematic || Static) X Dynamic
						gameObject2.setVelocityY(
							((lastGameObject2YVelocity * -1) + lastGameObject1YVelocity) * gameObject2.bounce.y - gravityY
						);

					} else if (gameObject1.bodyType === "D" && gameObject2.bodyType !== "D") { // Dynamic X (Kinematic || Static)
						gameObject1.setVelocityY(
							((lastGameObject1YVelocity * -1) + lastGameObject2YVelocity) * gameObject1.bounce.y - gravityY
						);
					}
				}

				// Horizontal
				if (collisionType === "LEFT" || collisionType === "RIGHT") {
					// console.log("LEFT/RIGHT");
					if (gameObject1.bodyType === "D" && gameObject2.bodyType === "D") { // Dynamic X Dynamic
						gameObject1.setVelocityX(
							lastGameObject2XVelocity * gameObject1.bounce.x - gravityX
						);
						gameObject2.setVelocityX(
							lastGameObject1XVelocity * gameObject2.bounce.x - gravityX
						);

					} else if (gameObject1.bodyType !== "D" && gameObject2.bodyType === "D") { // (Kinematic || Static) X Dynamic
						gameObject2.setVelocityX(
							((lastGameObject2XVelocity * -1) + lastGameObject1XVelocity) * gameObject2.bounce.x - gravityX
						);

					} else if (gameObject1.bodyType === "D" && gameObject2.bodyType !== "D") { // Dynamic X (Kinematic || Static)
						gameObject1.setVelocityX(
							((lastGameObject1XVelocity * -1) + lastGameObject2XVelocity) * gameObject1.bounce.x - gravityX
						);
					}
				}
			});
		});
	});
}

// ---- Lidar com a colisão detetando o lado
resolveCollision(gameObject1, gameObject2) {
	if (!this.overlapRectangleAndRectangle(gameObject1, gameObject2)) return;

	const dx = gameObject1.getCenterX() - gameObject2.getCenterX();// x difference between centers
	const dy = gameObject1.getCenterY() - gameObject2.getCenterY();// y difference between centers
	const aw = (gameObject1.width + gameObject2.width) * 0.5;// average width
	const ah = (gameObject1.height + gameObject2.hheight) * 0.5;// average height

	const lastGameObject1XVelocity = gameObject1.velocity.x;
	const lastGameObject1YVelocity = gameObject1.velocity.y;
	const lastGameObject2XVelocity = gameObject2.velocity.x;
	const lastGameObject2YVelocity = gameObject2.velocity.y;
	const gravityX = GlobalStateManagerInstance.gravity.x;
	const gravityY = GlobalStateManagerInstance.gravity.y;

	if (Math.abs(dx / gameObject2.width) > Math.abs(dy / gameObject2.height)) {
		console.log("X");
		// if (dx < 0) gameObject1.x = gameObject2.x - gameObject1.width;// left
		// else gameObject1.x = gameObject2.x + gameObject2.width; // right

		if (gameObject1.bodyType === "D" && gameObject2.bodyType === "D") { // Dynamic X Dynamic
			gameObject1.setVelocityX(
				lastGameObject2XVelocity * gameObject1.bounce.x - gravityX
			);
			gameObject2.setVelocityX(
				lastGameObject1XVelocity * gameObject2.bounce.x - gravityX
			);

		} else if (gameObject1.bodyType !== "D" && gameObject2.bodyType === "D") { // (Kinematic || Static) X Dynamic
			gameObject2.setVelocityX(
				((lastGameObject2XVelocity * -1) + lastGameObject1XVelocity) * gameObject2.bounce.x - gravityX
			);

		} else if (gameObject1.bodyType === "D" && gameObject2.bodyType !== "D") { // Dynamic X (Kinematic || Static)
			gameObject1.setVelocityX(
				((lastGameObject1XVelocity * -1) + lastGameObject2XVelocity) * gameObject1.bounce.x - gravityX
			);
		}

	} else {
		console.log("Y");
		// if (dy < 0) gameObject1.y = gameObject2.y - gameObject1.height; // top
		// else gameObject1.y = gameObject2.y + gameObject2.height; // bottom

		// Vertical
		if (gameObject1.bodyType === "D" && gameObject2.bodyType === "D") { // Dynamic X Dynamic
			gameObject1.setVelocityY(
				lastGameObject2YVelocity * gameObject1.bounce.y - gravityY
			);
			gameObject2.setVelocityY(
				lastGameObject1YVelocity * gameObject2.bounce.y - gravityY
			);

		} else if (gameObject1.bodyType !== "D" && gameObject2.bodyType === "D") { // (Kinematic || Static) X Dynamic
			gameObject2.setVelocityY(
				((lastGameObject2YVelocity * -1) + lastGameObject1YVelocity) * gameObject2.bounce.y - gravityY
			);

		} else if (gameObject1.bodyType === "D" && gameObject2.bodyType !== "D") { // Dynamic X (Kinematic || Static)
			gameObject1.setVelocityY(
				((lastGameObject1YVelocity * -1) + lastGameObject2YVelocity) * gameObject1.bounce.y - gravityY
			);
		}
	}
}


// ---- E mais um
