console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

const assets = {
	sprites: {
		// lorem: "https://picsum.photos/200/300",
		// lorem500: "https://picsum.photos/500",
		// lorem1000: "https://picsum.photos/1000",
		// lorem2000: "https://picsum.photos/2000",
		// lorem5000: "https://picsum.photos/5000",
	},
	fonts: {
		"PressStart2P": "./Press_Start_2P/PressStart2P-Regular.ttf",
		"Hurricane": "./Hurricane/Hurricane-Regular.ttf",
		"Rock3D": "https://fonts.gstatic.com/s/rock3d/v5/yYLp0hrL0PCo651513GnxxnRksddYfafVfsbrrIjEO_lgsJ9g9cKYA.3.woff2",
	},
}

class Game extends Impacto.Scene {
	start() {
		const myText = new Impacto.GameObjects.Text(400, 300, "Press Start 2P");
		myText.setFontFamily("PressStart2P");
		this.addChild(myText);

		const myText2 = new Impacto.GameObjects.Text(0, 0, "Rock3D")
			.setFontFamily("Rock3D");
		this.addChild(myText2);

		const myText3 = new Impacto.GameObjects.Text(100, 100, "Hurricane")
			.setFontFamily("Hurricane");
		this.addChild(myText3);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	assets,
	scene: Game,
});
