console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

const assets = {
	sprites: {
		Duke: "./Duke.png",
		Alien: "./Alien.png",
		// lorem: "https://picsum.photos/200/300",
		// lorem500: "https://picsum.photos/500",
		// lorem1000: "https://picsum.photos/1000",
		// lorem2000: "https://picsum.photos/2000",
		// lorem5000: "https://picsum.photos/5000",
	}
}

class Game extends Impacto.Scene {
	start() {
		const dukeSprite = new Impacto.GameObjects.Sprite(400, 300, "Duke");
		dukeSprite.setScale(2);
		this.addChild(dukeSprite);

		const dukeSprite2 = new Impacto.GameObjects.Sprite(400, 300, "Duke");
		this.addChild(dukeSprite2);

		// --
		const alien = new Impacto.GameObjects.Sprite(0, 0, "Alien")
			.setX(400)
			.setY(100)
			.setWidth(32)
			.setScale(2)
			.setOrigin(0.5, 0.5)
			.setFrame(10)

		alien.animations.add("Default", alien.getNumFramesByWidth(), 0, 100, false);
		console.log(alien.animations.get("Default"));
		alien.animations.play("Default");
		this.addChild(alien);

		setTimeout(() => {
			alien.animations.pause();
			setTimeout(() => {
				alien.animations.resume();
			}, 1000);
		}, 1000);

		// -- Alien 2
		const alien2 = new Impacto.GameObjects.Sprite(0, 0, "Alien", 0, 32);
		alien2.animations.add("Default2", alien2.getNumFramesByWidth() / 2, 16, 50);
		alien2.animations.play("Default2");
		this.addChild(alien2);
		console.log(alien2, alien2.animations.getAnimationsNames());

	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	assets,
	scene: Game,
});
