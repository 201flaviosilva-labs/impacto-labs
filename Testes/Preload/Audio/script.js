console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

const assets = {
	sprites: {
		// Duke: "./Duke.png",
		// Alien: "./Alien.png",
		// lorem: "https://picsum.photos/200/300",
		// lorem500: "https://picsum.photos/500",
		// lorem1000: "https://picsum.photos/1000",
		// lorem2000: "https://picsum.photos/2000",
		// lorem5000: "https://picsum.photos/5000",
	},
	audios: {
		// music: "./music.mp3",
		// sound: "./sound.mp3",
		Explosion: "./Explosion.wav",
	},
}

class Game extends Impacto.Scene {
	start() {
		const rect = new Impacto.GameObjects.Rectangle(0, 0, 100, 100, "red");
		this.addChild(rect);

		const explosionAudio = new Impacto.GameObjects.AudioPlay("Explosion", true, false, 0.1, false, 500);
		setTimeout(() => {
			explosionAudio.stop();
			setTimeout(() => {
				explosionAudio.play();
				// 	setTimeout(() => {
				// 		explosionAudio.pause();
				// 		setTimeout(() => {
				// 			explosionAudio.resume();
				// 			explosionAudio.playOnce();
				// 		}, 1000);
				// 	}, 1000);
			}, 1000);
		}, 1000);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	assets,
	scene: Game,
});
