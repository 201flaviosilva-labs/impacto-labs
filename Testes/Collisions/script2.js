console.clear();
// import Impacto from "../../Build/Impacto.esm.js";
import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.circle1 = new Impacto.Impacto.GameObjects.PhysicsCircle(400, 300, 25, "#0000ff");
		this.addChild(this.circle1);

		this.circle2 = new Impacto.PhysicsCircle(0, 0, 33, "#00ff00");
		this.addChild(this.circle2);
	}

	update() {
		const mouse = Impacto.Inputs.Mouse.getPosition();
		this.circle2.setPosition(mouse.x, mouse.y);

		if (Impacto.Utils.CollisionDetection.overlapCircleAndCircle(this.circle1, this.circle2)) {
			this.circle1.setFillColor("#ff0000");
			this.circle2.setFillColor("#ff0000");
		} else {
			this.circle1.setFillColor("#0000ff");
			this.circle2.setFillColor("#00ff00");
		}
	}

	posRender() {
		const ctx = Impacto.Utils.Canvas.context;

		// Draw a line of the distance between the two circles
		ctx.fillStyle = "#f8f8f8";
		ctx.beginPath();
		ctx.moveTo(this.circle2.getCenterX(), this.circle2.getCenterY());
		ctx.lineTo(this.circle1.getCenterX(), this.circle1.getCenterY());
		ctx.stroke();
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
