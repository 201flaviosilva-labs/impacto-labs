console.clear();
import Impacto from "../../../Build/Impacto.esm.js";

class Game extends Impacto.Scene {
	start() {
		this.rectStoped = new Impacto.Rectangle(580, 100, 10, 400, "#ffff00");
		this.rectStoped.setCollisionWorldBounds(true);
		this.rectStoped.setBounce(1);
		this.addChild(this.rectStoped);
		// this.collisions.layer1.push(this.rectStoped);

		this.rect = new Impacto.Rectangle(400, 300, 50, 50, "#ff0000");
		this.rect.setCollisionWorldBounds(true);
		this.rect.setBounce(1);
		this.rect.setVelocity(5, -5);
		this.addChild(this.rect);
		// this.collisions.layer1.push(this.rect);
	}

	update() {
		// Platform Game
		const speed = 10;
		const { left, up, right, down } = Impacto.Inputs.KeyBoard.keys;
		if (Impacto.Inputs.KeyBoard.isKeyPressed(left) || Impacto.Inputs.KeyBoard.isKeyPressed("a")) this.rect.setVelocityX(-speed);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(right) || Impacto.Inputs.KeyBoard.isKeyPressed("d")) this.rect.setVelocityX(speed);
		else this.rect.setVelocityX(0);
		if (Impacto.Inputs.KeyBoard.isKeyPressed(up) || Impacto.Inputs.KeyBoard.isKeyPressed("w")) this.rect.setVelocityY(-speed);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(down) || Impacto.Inputs.KeyBoard.isKeyPressed("s")) this.rect.setVelocityY(speed);
		else this.rect.setVelocityY(0);

		console.log(this.overlappingArea(this.rect.getBottomLeft(), this.rect.getTopRight(), this.rectStoped.getBottomLeft(), this.rectStoped.getTopRight()));
	}

	overlappingArea(l1, r1, l2, r2) {
		let x = 0;
		let y = 1;


		// Area of 1st Rectangle
		let area1 = Math.abs(l1.x - r1.x) * Math.abs(l1.y - r1.y);

		// Area of 2nd Rectangle
		let area2 = Math.abs(l2.x - r2.x) * Math.abs(l2.y - r2.y);

		// Length of intersecting part i.e
		// start from max(l1[x], l2[x]) of
		// x-coordinate and end at min(r1[x],
		// r2[x]) x-coordinate by subtracting
		// start from end we get required
		// lengths
		let x_dist = (Math.min(r1.x, r2.x) - Math.max(l1.x, l2.x));
		let y_dist = (Math.min(r1.y, r2.y) - Math.max(l1.y, l2.y));
		let areaI = 0;
		if (x_dist > 0 && y_dist > 0) areaI = x_dist * y_dist

		return (area1 + area2 - areaI)
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
