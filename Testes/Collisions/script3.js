console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.rectStoped = new Impacto.GameObjects.PhysicsRectangle(580, 50, 10, 500, "#ffff00"); // Horizontal
		this.rectStoped.setCollisionWorldBounds(true);
		this.rectStoped.setBounce(1);
		// this.rectStoped.setVelocity(-5);
		this.addChild(this.rectStoped);
		this.collisions.layer1.push(this.rectStoped);

		const rectStoped2 = new Impacto.GameObjects.PhysicsRectangle(100, 10, 600, 10, "#ffff00"); // Vertical
		rectStoped2.setCollisionWorldBounds(true);
		rectStoped2.setBounce(1);
		rectStoped2.setVelocity(5);
		// rectStoped2.setKinematicBody();
		this.addChild(rectStoped2);
		this.collisions.layer1.push(rectStoped2);

		const rectStoped3 = new Impacto.GameObjects.PhysicsRectangle(250, 295, 100, 10, "#ffff00");
		rectStoped3.setCollisionWorldBounds(true);
		rectStoped3.setBounce(1);
		rectStoped3.setVelocity(5);
		// rectStoped3.setKinematicBody();
		this.addChild(rectStoped3);
		this.collisions.layer1.push(rectStoped3);

		this.rect = new Impacto.GameObjects.PhysicsRectangle(400, 300, 100, 100, "#ff0000");
		this.rect.setCollisionWorldBounds(true);
		this.rect.setBounce(1);
		// rect.setVelocity(5, -5);
		this.rect.setVelocity(5, -5);
		// rect.setVelocity(0, -5);
		// rect.setVelocity(-1, 0);
		this.addChild(this.rect);
		this.collisions.layer1.push(this.rect);

		this.collisionRect = new Impacto.GameObjects.PhysicsRectangle(0, 0, 0, 0, "#000000");
		this.collisionRect.setZ(100);
		this.addChild(this.collisionRect);
	}

	update() {
		return;
		this.rect.setPosition(Impacto.Inputs.Mouse.getPosition().x - this.rect.width / 2, Impacto.Inputs.Mouse.getPosition().y - this.rect.height / 2);

		if (Impacto.Utils.CollisionDetection.overlapRectangleAndRectangle(this.rect, this.rectStoped)) {
			this.rect.setFillColor("#00ff00");
			this.rectStoped.setFillColor("#00ff00");
			this.rect.setVelocity(0);
			this.rectStoped.setVelocity(0);

			const collisionBoundsRect = Impacto.Utils.CollisionDetection.getBoundsOfTwoOverlappingRectangles(this.rect, this.rectStoped);
			this.collisionRect.setPosition(collisionBoundsRect.x, collisionBoundsRect.y);
			this.collisionRect.setSize(collisionBoundsRect.width, collisionBoundsRect.height);
		} else {
			this.rect.setFillColor("#ff0000");
			this.rectStoped.setFillColor("#0000ff");
		}
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
