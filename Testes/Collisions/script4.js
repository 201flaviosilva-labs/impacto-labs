console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.rectStoped = new Impacto.Rectangle(580, 50, 10, 500, "#ffff00"); // Horizontal
		this.rectStoped.setCollisionWorldBounds(true);
		this.rectStoped.setBounce(1);
		// this.rectStoped.setKinematicBody();
		this.addChild(this.rectStoped);
		this.collisions.layer1.push(this.rectStoped);

		// this.rectStoped = new Impacto.Rectangle(50, 10, 700, 10, "#ffff00"); // Horizontal
		// this.rectStoped.setCollisionWorldBounds(true);
		// this.rectStoped.setBounce(1);
		// this.addChild(this.rectStoped);
		// this.collisions.layer1.push(this.rectStoped);

		this.rect = new Impacto.Rectangle(400, 300, 100, 100, "#ff0000");
		this.rect.setCollisionWorldBounds(true);
		this.rect.setBounce(1);
		this.rect.setVelocity(5, -5);
		this.addChild(this.rect);
		this.collisions.layer1.push(this.rect);
	}

	update() {
		return;
		// this.rect.setPosition(Impacto.Inputs.Mouse.getPosition().x - this.rect.width / 2, Impacto.Inputs.Mouse.getPosition().y - this.rect.height / 2);

		const dx = this.rect.getCenterX() - this.rectStoped.getCenterX();// x difference between centers
		const dy = this.rect.getCenterY() - this.rectStoped.getCenterY();// y difference between centers
		const aw = (this.rect.width + this.rectStoped.width) * 0.5;// average width
		const ah = (this.rect.height + this.rectStoped.hheight) * 0.5;// average height

		/* If either distance is greater than the average dimension there is no collision. */
		if (!Impacto.Utils.CollisionDetection.overlapRectangleAndRectangle(this.rect, this.rectStoped)) return false;

		/* To determine which region of this rectangle the rect's center
		point is in, we have to account for the scale of the this rectangle.
		To do that, we divide dx and dy by it's width and height respectively. */
		// console.log(dx / this.rect.width, dy / this.rect.height);
		if (Math.abs(dx / this.rectStoped.width) > Math.abs(dy / this.rectStoped.height)) {
			console.log("x");
			if (dx < 0) this.rect.x = this.rectStoped.x - this.rect.width;// left
			else this.rect.x = this.rectStoped.x + this.rectStoped.width; // right
			this.rect.setVelocityX(this.rect.velocity.x * -1);

		} else {
			console.log("y");
			if (dy < 0) this.rect.y = this.rectStoped.y - this.rect.height; // top
			else this.rect.y = this.rectStoped.y + this.rectStoped.height; // bottom
			this.rect.setVelocityY(this.rect.velocity.y * -1);
		}

		return true;
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
