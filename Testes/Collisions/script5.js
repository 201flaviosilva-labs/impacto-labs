console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.cricle1 = new Impacto.Rectangle(100, 100, 150, 150, "red");
		this.cricle1.setCollisionWorldBounds(true);
		this.cricle1.setBounce(1);
		this.cricle1.setVelocity(5);
		this.addChild(this.cricle1);
		this.collisions.layer1.push(this.cricle1);

		this.cricle2 = new Impacto.Rectangle(400, 300, 100, 100, "blue");
		this.cricle2.setCollisionWorldBounds(true);
		this.cricle2.setBounce(1);
		this.cricle2.setVelocity(-2.5);
		this.addChild(this.cricle2);
		this.collisions.layer1.push(this.cricle2);
	}

	update() {
		if (Impacto.Utils.CollisionDetection.overlapRectangleAndRectangle(this.cricle1, this.cricle2)) {
			this.collisionResponse(this.cricle1, this.cricle2);
		}
	}

	collisionResponse(obj1, obj2) {
		// const vCollision = { x: obj2.x - obj1.x, y: obj2.y - obj1.y };
		// const distance = Math.sqrt((obj2.x - obj1.x) * (obj2.x - obj1.x) + (obj2.y - obj1.y) * (obj2.y - obj1.y));
		// const vCollisionNorm = { x: vCollision.x / distance, y: vCollision.y / distance };
		// const vRelativeVelocity = { x: obj1.velocity.x - obj2.velocity.x, y: obj1.velocity.y - obj2.velocity.y };
		// const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;

		// if (speed < 0) {
		// 	return;
		// }

		// const impulse = 2 * speed / (1 + 1);
		// obj1.velocity.x -= (impulse * 1 * vCollisionNorm.x);
		// obj1.velocity.y -= (impulse * 1 * vCollisionNorm.y);
		// obj2.velocity.x += (impulse * 1 * vCollisionNorm.x);
		// obj2.velocity.y += (impulse * 1 * vCollisionNorm.y);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
