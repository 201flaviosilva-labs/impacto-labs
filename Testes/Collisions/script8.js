console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		// const w2 = Impacto.Utils.Math.randomInt(10, 200);
		// const h2 = Impacto.Utils.Math.randomInt(10, 200);
		const w2 = 100;
		const h2 = 100;
		this.rect2 = new Impacto.Rectangle(0, 0, w2, h2, "#ff0000");
		this.addChild(this.rect2);
		// this.collisions.layer1.push(this.rect2);

		// const w1 = Impacto.Utils.Math.randomInt(10, 200);
		// const h1 = Impacto.Utils.Math.randomInt(10, 200);
		const w1 = 75;
		const h1 = 200;
		this.rect1 = new Impacto.Rectangle(400 - w1 / 2, 300 - h1 / 2, w1, h1, "#00ffff");
		this.addChild(this.rect1);
		// this.collisions.layer1.push(this.rect1);

		this.collisionPoints = [];
	}

	update() {
		const mouse = Impacto.Inputs.Mouse.getPosition();
		this.rect2.setPosition(mouse.x - this.rect2.width / 2, mouse.y - this.rect2.height / 2);

		if (Impacto.Inputs.Mouse.isButtonDown(0)) {
			const w1 = Impacto.Utils.Math.randomInt(10, 200);
			const h1 = Impacto.Utils.Math.randomInt(10, 200);
			this.rect1.setSize(w1, h1);
		}

		if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.up)) this.rect1.setHeight(this.rect1.height - 10);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.down)) this.rect1.setHeight(this.rect1.height + 10);

		if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.left)) this.rect1.setWidth(this.rect1.width - 10);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.right)) this.rect1.setWidth(this.rect1.width + 10);
		this.rect1.setPosition(400 - this.rect1.width / 2, 300 - this.rect1.height / 2);

		if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.w)) this.rect2.setHeight(this.rect2.height - 10);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.s)) this.rect2.setHeight(this.rect2.height + 10);

		if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.a)) this.rect2.setWidth(this.rect2.width - 10);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(Impacto.Inputs.KeyBoard.keys.d)) this.rect2.setWidth(this.rect2.width + 10);

		this.collisionPoints = Impacto.Utils.CollisionDetection.detectRectangleAndRectangleCollisionDirections(this.rect1, this.rect2);

		Impacto.Utils.CollisionDetection.collisionPerpetrationResolve(this.rect1, this.rect2);

		if (Impacto.Utils.CollisionDetection.checkOverlap(this.rect1, this.rect2)) {
			this.rect1.setFillColor("#00ff00");
			this.rect2.setFillColor("#00ff00");
		} else {
			this.rect1.setFillColor("#00ffff");
			this.rect2.setFillColor("#ff0000");
		}
	}

	posRender(ctx) {
		const mouse = Impacto.Inputs.Mouse.getPosition();

		// Draw a line of the distance between the two rectangles
		ctx.fillStyle = "#f8f8f8";
		ctx.beginPath();
		ctx.moveTo(this.rect2.getCenterX(), this.rect2.getCenterY());
		ctx.lineTo(this.rect1.getCenterX(), this.rect1.getCenterY());
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(this.rect2.getTopLeft().x, this.rect2.getTopLeft().y);
		ctx.lineTo(this.rect2.getBottomRight().x, this.rect2.getBottomRight().y);
		ctx.lineTo(this.rect2.getTopRight().x, this.rect2.getTopRight().y);
		ctx.lineTo(this.rect2.getBottomLeft().x, this.rect2.getBottomLeft().y);

		ctx.lineTo(this.rect2.getTopLeft().x, this.rect2.getTopLeft().y);
		ctx.lineTo(this.rect2.getLeftCenter().x, this.rect2.getLeftCenter().y);
		ctx.lineTo(this.rect2.getRightCenter().x, this.rect2.getRightCenter().y);

		ctx.lineTo(this.rect2.getTopRight().x, this.rect2.getTopRight().y);
		ctx.lineTo(this.rect2.getTopCenter().x, this.rect2.getTopCenter().y);
		ctx.lineTo(this.rect2.getBottomCenter().x, this.rect2.getBottomCenter().y);
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(this.rect1.getTopLeft().x, this.rect1.getTopLeft().y);
		ctx.lineTo(this.rect1.getBottomRight().x, this.rect1.getBottomRight().y);
		ctx.lineTo(this.rect1.getTopRight().x, this.rect1.getTopRight().y);
		ctx.lineTo(this.rect1.getBottomLeft().x, this.rect1.getBottomLeft().y);

		ctx.lineTo(this.rect1.getTopLeft().x, this.rect1.getTopLeft().y);
		ctx.lineTo(this.rect1.getLeftCenter().x, this.rect1.getLeftCenter().y);
		ctx.lineTo(this.rect1.getRightCenter().x, this.rect1.getRightCenter().y);

		ctx.lineTo(this.rect1.getTopRight().x, this.rect1.getTopRight().y);
		ctx.lineTo(this.rect1.getTopCenter().x, this.rect1.getTopCenter().y);
		ctx.lineTo(this.rect1.getBottomCenter().x, this.rect1.getBottomCenter().y);
		ctx.stroke();


		// Draw the mouse rectangle
		ctx.fillStyle = null;
		ctx.strokeStyle = "#000000";
		ctx.strokeRect(mouse.x - this.rect2.width / 2, mouse.y - this.rect2.height / 2, this.rect2.width, this.rect2.height);

		// Draw the Collision points
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStyle = "#000000";
		ctx.font = "30px Arial";
		ctx.fillText(JSON.stringify(this.collisionPoints), this.rect2.getCenterX(), 50);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
