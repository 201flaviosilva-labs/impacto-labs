console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.rects = [];

		const bodyTypes = ["S", "D", "T"];

		for (let i = 0; i < 10; i++) {
			const rect = new Impacto.Rectangle(400, 300, Impacto.Utils.Math.randomInt(20, 200), Impacto.Utils.Math.randomInt(20, 200), Impacto.Utils.Math.randomColor());
			rect.setCollisionWorldBounds(true);
			rect.setBounce(1);
			rect.setVelocity(Impacto.Utils.Math.randomInt(-5, 5), Impacto.Utils.Math.randomInt(-5, 5));
			rect.setRandomPosition();
			rect.setBodyType(bodyTypes[Impacto.Utils.Math.randomInt(0, bodyTypes.length)]);
			this.addChild(rect);
			this.collisions.layer1.push(rect);

			this.rects.push(rect);
		}
	}

	posRender(ctx) {
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStyle = "#000000";
		ctx.font = "30px Arial";
		this.rects.forEach(rect => {
			ctx.fillText(rect.bodyType, rect.getCenterX(), rect.getCenterY());
		});
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
