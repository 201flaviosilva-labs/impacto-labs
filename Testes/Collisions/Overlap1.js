console.clear();
// import Impacto from "../../Build/Impacto.esm.js";
import Impacto from "../../../src/Impacto.js";
import drawAllPoints from "../Points.js";

class Game extends Impacto.Scene {
	start() {
		this.rects = [];
		// Horizontal
		const collisionRect1 = new Impacto.GameObjects.PhysicsRectangle(100, 250, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ff0000");
		collisionRect1.setCollisionWorldBounds(true);
		collisionRect1.setBounce(1);
		collisionRect1.setVelocityX(2.5);
		collisionRect1.setRandomPosition();
		this.addChild(collisionRect1);

		const collisionRect2 = new Impacto.GameObjects.PhysicsRectangle(600, 250, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#00ff00");
		collisionRect2.setCollisionWorldBounds(true);
		collisionRect2.setBounce(1);
		collisionRect2.setVelocityX(-5);
		collisionRect2.setRandomPosition();
		this.addChild(collisionRect2);

		const collisionRect3 = new Impacto.GameObjects.PhysicsRectangle(700, 250, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#0000ff");
		collisionRect3.setCollisionWorldBounds(true);
		collisionRect3.setBounce(1);
		collisionRect3.setVelocityX(10);
		collisionRect3.setRandomPosition();
		this.addChild(collisionRect3);

		const collisionRect4 = new Impacto.GameObjects.PhysicsRectangle(300, 100, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#000000");
		collisionRect4.setCollisionWorldBounds(true);
		collisionRect4.setBounce(1);
		collisionRect4.setVelocityX(10);
		collisionRect4.setRandomPosition();
		this.addChild(collisionRect4);

		// Vertical
		const collisionRectVertical1 = new Impacto.GameObjects.PhysicsRectangle(250, 10, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ffff00");
		collisionRectVertical1.setCollisionWorldBounds(true);
		collisionRectVertical1.setBounce(1);
		collisionRectVertical1.setVelocityY(2.5);
		collisionRectVertical1.setRandomPosition();
		this.addChild(collisionRectVertical1);

		const collisionRectVertical2 = new Impacto.GameObjects.PhysicsRectangle(250, 200, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#00ffff");
		collisionRectVertical2.setCollisionWorldBounds(true);
		collisionRectVertical2.setBounce(1);
		collisionRectVertical2.setVelocityY(-5);
		collisionRectVertical2.setRandomPosition();
		this.addChild(collisionRectVertical2);

		const collisionRectVertical3 = new Impacto.GameObjects.PhysicsRectangle(250, 500, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ff00ff");
		collisionRectVertical3.setCollisionWorldBounds(true);
		collisionRectVertical3.setBounce(1);
		collisionRectVertical3.setVelocityY(10);
		collisionRectVertical3.setRandomPosition();
		this.addChild(collisionRectVertical3);

		const collisionRectVertical4 = new Impacto.GameObjects.PhysicsRectangle(500, 100, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ffffff");
		collisionRectVertical4.setCollisionWorldBounds(true);
		collisionRectVertical4.setBounce(1);
		collisionRectVertical4.setVelocityY(10);
		collisionRectVertical4.setRandomPosition();
		this.addChild(collisionRectVertical4);

		this.rects.push(collisionRect1, collisionRect2, collisionRect3, collisionRect4, collisionRectVertical1, collisionRectVertical2, collisionRectVertical3, collisionRectVertical4);

		for (let i = 0; i < this.rects.length; i++) {
			this.rects[i].setOrigin(Math.random(), Math.random());
			// this.rects[i].setOrigin(0.5);
		}
	}

	posRender(ctx) {
		for (let i = 0; i < this.rects.length; i++) {
			drawAllPoints(ctx, this.rects[i]);
			for (let j = i + 1; j < this.rects.length; j++) {
				if (i < j) {
					if (Impacto.Utils.CollisionDetection.checkOverlap(this.rects[i], this.rects[j])) {
						const bounds = Impacto.Utils.CollisionDetection.getBoundsOfTwoOverlappingRectangles(this.rects[i].getBounds(), this.rects[j].getBounds());

						ctx.strokeStyle = "#00ff00";
						ctx.fillStyle = "#000000";
						ctx.strokeRect(bounds.x, bounds.y, bounds.width, bounds.height);
						ctx.fillRect(bounds.x, bounds.y, bounds.width, bounds.height);
					}
				}
			}
		}

	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
