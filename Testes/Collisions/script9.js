console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		// Horizontal
		this.createNewCollisionLayer("layer1");
		const collisionRect1 = new Impacto.Rectangle(100, 250, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ff0000");
		collisionRect1.setCollisionWorldBounds(true);
		collisionRect1.setBounce(1);
		collisionRect1.setVelocityX(2.5);
		collisionRect1.setRandomPosition();
		this.addChild(collisionRect1);
		this.collisions.layer1.push(collisionRect1);

		const collisionRect2 = new Impacto.Rectangle(600, 250, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#00ff00");
		collisionRect2.setCollisionWorldBounds(true);
		collisionRect2.setBounce(1);
		collisionRect2.setVelocityX(-5);
		collisionRect2.setRandomPosition();
		this.addChild(collisionRect2);
		this.collisions.layer1.push(collisionRect2);

		const collisionRect3 = new Impacto.Rectangle(700, 250, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#0000ff");
		collisionRect3.setCollisionWorldBounds(true);
		collisionRect3.setBounce(1);
		collisionRect3.setVelocityX(10);
		collisionRect3.setRandomPosition();
		this.addChild(collisionRect3);
		this.collisions.layer1.push(collisionRect3);

		const collisionRect4 = new Impacto.Rectangle(300, 100, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#000000");
		collisionRect4.setCollisionWorldBounds(true);
		collisionRect4.setBounce(1);
		collisionRect4.setVelocityX(10);
		collisionRect4.setRandomPosition();
		this.addChild(collisionRect4);
		this.collisions.layer1.push(collisionRect4);

		// Vertical
		const collisionRectVertical1 = new Impacto.Rectangle(250, 10, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ffff00");
		collisionRectVertical1.setCollisionWorldBounds(true);
		collisionRectVertical1.setBounce(1);
		collisionRectVertical1.setVelocityY(2.5);
		collisionRectVertical1.setRandomPosition();
		this.addChild(collisionRectVertical1);
		this.collisions.layer1.push(collisionRectVertical1);

		const collisionRectVertical2 = new Impacto.Rectangle(250, 200, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#00ffff");
		collisionRectVertical2.setCollisionWorldBounds(true);
		collisionRectVertical2.setBounce(1);
		collisionRectVertical2.setVelocityY(-5);
		collisionRectVertical2.setRandomPosition();
		this.addChild(collisionRectVertical2);
		this.collisions.layer1.push(collisionRectVertical2);

		const collisionRectVertical3 = new Impacto.Rectangle(250, 500, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ff00ff");
		collisionRectVertical3.setCollisionWorldBounds(true);
		collisionRectVertical3.setBounce(1);
		collisionRectVertical3.setVelocityY(10);
		collisionRectVertical3.setRandomPosition();
		this.addChild(collisionRectVertical3);
		this.collisions.layer1.push(collisionRectVertical3);

		const collisionRectVertical4 = new Impacto.Rectangle(500, 100, Impacto.Utils.Math.randomInt(10, 200), Impacto.Utils.Math.randomInt(10, 200), "#ffffff");
		collisionRectVertical4.setCollisionWorldBounds(true);
		collisionRectVertical4.setBounce(1);
		collisionRectVertical4.setVelocityY(10);
		collisionRectVertical4.setRandomPosition();
		this.addChild(collisionRectVertical4);
		this.collisions.layer1.push(collisionRectVertical4);
	}

	update() {
		this.collisions.layer1.forEach(gameObject => {
			gameObject.isColliding = false;
			gameObject.setFillColor("#ffffff");
		});

		this.collisions.layer1.forEach((gameObject1, index1) => {
			this.collisions.layer1.forEach((gameObject2, index2) => {
				if (index1 >= index2) return;
				if (Impacto.Utils.CollisionDetection.checkOverlap(gameObject1, gameObject2)) {
					gameObject1.isColliding = true;
					gameObject2.isColliding = true;
				}
			});
		});

		this.collisions.layer1.forEach(gameObject => {
			if (gameObject.isColliding) {
				gameObject.setFillColor("#ff0000");
			}
		});
	}

	posRender(ctx) {
		this.collisions.layer1.forEach((gameObject1, index1) => {
			this.collisions.layer1.forEach((gameObject2, index2) => {
				if (index1 >= index2) return;
				if (Impacto.Utils.CollisionDetection.checkOverlap(gameObject1, gameObject2)) {
					const speed = this.collisionResponseDynamicRectRectReturnSpeed(gameObject1, gameObject2);
					Impacto.Utils.Canvas.drawText(
						JSON.stringify(speed),
						gameObject1.x + gameObject1.width / 2,
						gameObject1.y + gameObject1.height / 2,
						"#00ff00");
				}
			});
		});

	}

	collisionResponseDynamicRectRectReturnSpeed(dynamicReact1, dynamicReact2) {
		// https://spicyyoghurt.com/tutorials/html5-javascript-game-development/collision-detection-physics

		const vCollision = { x: dynamicReact2.x - dynamicReact1.x, y: dynamicReact2.y - dynamicReact1.y };
		const distance = Math.sqrt((dynamicReact2.x - dynamicReact1.x) * (dynamicReact2.x - dynamicReact1.x) + (dynamicReact2.y - dynamicReact1.y) * (dynamicReact2.y - dynamicReact1.y));
		const vCollisionNorm = { x: vCollision.x / distance, y: vCollision.y / distance };
		const vRelativeVelocity = { x: dynamicReact1.velocity.x - dynamicReact2.velocity.x, y: dynamicReact1.velocity.y - dynamicReact2.velocity.y };
		const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;

		const impulse = 2 * speed / (dynamicReact1.mass + dynamicReact2.mass);
		return { speed, impulse };
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
