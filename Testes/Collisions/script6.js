console.clear();
import Impacto from "../../Build/Impacto.esm.js";
// import Impacto from "../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.circles = [];

		for (let i = 0; i < 10; i++) {
			const circle = new Impacto.Circle(400, 300, Impacto.Utils.Math.randomInt(25, 100), Impacto.Utils.Math.randomColor());
			circle.setCollisionWorldBounds(true);
			circle.setBounce(1);
			circle.setVelocity(Impacto.Utils.Math.randomInt(-5, 5), Impacto.Utils.Math.randomInt(-5, 5));
			circle.setRandomPosition();
			// circle.setBodyType(Math.random() > 0.5 ? "D" : "S");
			this.addChild(circle);

			this.circles.push(circle);
		}

		this.circles.map(circle => {
			console.log(circle.name);
		}
		);
	}

	update() {
		for (let i = 0; i < this.circles.length; i++) {
			for (let j = i + 1; j < this.circles.length; j++) {
				if (Impacto.Utils.CollisionDetection.checkOverlap(this.circles[i], this.circles[j]) && j > i) {
					// this.resolveCollision(this.circles[i], this.circles[j]);
					// this.simpleCollisionPernetationResolve(this.circles[i], this.circles[j]);
					// this.simpleResolveCollision(this.circles[i], this.circles[j]);
					this.resolveCircle(this.circles[i], this.circles[j]);
				}
			}
		}
	}

	resolveCircle(c1, c2) {
		// https://stackoverflow.com/questions/45910915/circle-to-circle-collision-response-not-working-as-expected
		const obj1 = c1;
		const obj2 = c2;
		// get the direction and velocity of each ball
		const v1 = Math.sqrt(c1.velocity.x * c1.velocity.x + c1.velocity.y * c1.velocity.y);
		const v2 = Math.sqrt(c2.velocity.x * c2.velocity.x + c2.velocity.y * c2.velocity.y);

		// get the direction of travel of each ball
		const d1 = Math.atan2(c1.velocity.y, c1.velocity.x);
		const d2 = Math.atan2(c2.velocity.y, c2.velocity.x);

		// get the direction from ball1 center to ball2 cenet
		const dx = obj2.x - obj1.x;
		const dy = obj2.y - obj1.y;
		const dist = Math.sqrt(dx * dx + dy * dy);
		const nx = dx / dist;
		const ny = dy / dist;
		let cDir = Math.atan2(ny, nx);

		// You will also need a mass. You could use the area of a circle, or the
		// volume of a sphere to get the mass of each ball with its radius
		// this will make them react more realistically
		// An approximation is good as it is the ratio not the mass that is important
		// Thus ball are spheres. Volume is the cubed radius
		const m1 = Math.pow(c1.radius, 3);
		const m2 = Math.pow(c2.radius, 3);


		const mm = m1 - m2;
		const mmt = m1 + m2;
		const v1s = v1 * Math.sin(d1 - cDir);

		const cp = Math.cos(cDir);
		const sp = Math.sin(cDir);
		var cdp1 = v1 * Math.cos(d1 - cDir);
		var cdp2 = v2 * Math.cos(d2 - cDir);
		const cpp = Math.cos(cDir + Math.PI / 2)
		const spp = Math.sin(cDir + Math.PI / 2)

		var t = (cdp1 * mm + 2 * m2 * cdp2) / mmt;
		obj1.dx = t * cp + v1s * cpp;
		obj1.dy = t * sp + v1s * spp;
		cDir += Math.PI;
		const v2s = v2 * Math.sin(d2 - cDir);
		cdp1 = v1 * Math.cos(d1 - cDir);
		cdp2 = v2 * Math.cos(d2 - cDir);
		t = (cdp2 * -mm + 2 * m1 * cdp1) / mmt;
		obj2.dx = t * -cp + v2s * -cpp;
		obj2.dy = t * -sp + v2s * -spp
	}


	simpleCollisionPernetationResolve(circle1, circle2) {
		const v1 = circle1.velocity;
		const v2 = circle2.velocity;

		const v1n = { x: v1.x / Math.abs(v1.x), y: v1.y / Math.abs(v1.y) };
		const v2n = { x: v2.x / Math.abs(v2.x), y: v2.y / Math.abs(v2.y) };

		const v1t = { x: v1.x * v1n.x, y: v1.y * v1n.y };
		const v2t = { x: v2.x * v2n.x, y: v2.y * v2n.y };

		circle1.setPosition(v1n.x, v1n.y);
		circle2.setPosition(v2n.x, v2n.y);
	}

	simpleResolveCollision(circle1, circle2) {
		const m1 = circle1.radius;
		const m2 = circle2.radius;

		const v1 = { x: circle1.velocity.x, y: circle1.velocity.y };
		const v2 = { x: circle2.velocity.x, y: circle2.velocity.y };

		const vFinal1 = { x: (m1 - m2) / (m1 + m2) * v1.x + (2 * m2 / (m1 + m2)) * v2.x, y: v1.y };
		const vFinal2 = { x: (m1 - m2) / (m1 + m2) * v2.x + (2 * m2 / (m1 + m2)) * v1.x, y: v2.y };


		// let newVelX1 = (vel1.vX * (m1 - m2) + (2 * m2 * vel2.vX)) / (m1 + m2);
		// let newVelY1 = (vel1.vY * (m1 - m2) + (2 * m2 * vel2.vY)) / (m1 + m2);

		// let newVelX2 = (vel2.vX * (m1 - m2) + (2 * m1 * vel1.vX)) / (m1 + m2);
		// let newVelY2 = (vel2.vY * (m1 - m2) + (2 * m1 * vel1.vY)) / (m1 + m2);

		circle1.setVelocity(vFinal1.x, vFinal1.y);
		circle2.setVelocity(vFinal2.x, vFinal2.y);
	}

	rotate(velocity, angle) {
		const rotatedVelocities = {
			x: velocity.x * Math.cos(angle) - velocity.y * Math.sin(angle),
			y: velocity.x * Math.sin(angle) + velocity.y * Math.cos(angle)
		};

		return rotatedVelocities;
	}

	resolveCollision(circle1, circle2) {
		const xVelocityDiff = circle1.velocity.x - circle2.velocity.x;
		const yVelocityDiff = circle1.velocity.y - circle2.velocity.y;

		const xDist = circle2.x - circle1.x;
		const yDist = circle2.y - circle1.y;

		// Prevent accidental overlap of particles
		if (xVelocityDiff * xDist + yVelocityDiff * yDist >= 0) {

			// Grab angle between the two colliding particles
			const angle = -Math.atan2(circle2.y - circle1.y, circle2.x - circle1.x);

			// Store mass in var for better readability in collision equation
			const m1 = circle1.radius;
			const m2 = circle2.radius;

			// Velocity before equation
			const u1 = this.rotate(circle1.velocity, angle);
			const u2 = this.rotate(circle2.velocity, angle);

			// Velocity after 1d collision equation
			const v1 = { x: u1.x * (m1 - m2) / (m1 + m2) + u2.x * 2 * m2 / (m1 + m2), y: u1.y };
			const v2 = { x: u2.x * (m1 - m2) / (m1 + m2) + u1.x * 2 * m2 / (m1 + m2), y: u2.y };

			// Final velocity after rotating axis back to original location
			const vFinal1 = this.rotate(v1, -angle);
			const vFinal2 = this.rotate(v2, -angle);

			// Swap particle velocities for realistic bounce effect
			circle1.velocity.x = vFinal1.x;
			circle1.velocity.y = vFinal1.y;
			circle2.velocity.x = vFinal2.x;
			circle2.velocity.y = vFinal2.y;
		}
	}

	posRender(ctx) {
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStyle = "#000000";
		ctx.font = "30px Arial";
		this.circles.forEach(circle => {
			ctx.fillText(circle.bodyType, circle.x, circle.y);
		});
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
