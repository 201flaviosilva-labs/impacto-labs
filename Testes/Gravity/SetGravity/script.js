console.clear();
// import Impacto from "../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		for (let i = 0; i < 10; i++) {
			const bounce = Math.random();
			const color = Impacto.Utils.Math.randomColor();

			const rect = new Impacto.GameObjects.PhysicsRectangle(0, 0, 50, 50, color)
				.setBounce(bounce)
				.setCollisionWorldBounds(true)
				.setRandomPosition()
				.setOrigin(0.5);
			this.addChild(rect);

			Impacto.State.Global.setGravity({ x: 10, y: -10, });
		}
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: -1, y: 2, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
