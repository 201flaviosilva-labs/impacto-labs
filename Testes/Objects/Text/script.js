console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";
import TextOptions from "../../../../src/GameObjects/Text/Options.js";

class Game extends Impacto.Scene {
	start() {
		this.txt = new Impacto.GameObjects.Text(100, 100, "Hello World!")
			// .setOrigin(0.5)
			.setPosition(400, 300)
			// .setText("Hello Beep! - %& World! \" aksb")
			// .setText("asjdb aDjdbajsbdja Asbdjabs")
			.setFillColor("#ff0000")
			.setStrokeColor("#00ff00")
			.setAlignVertical("middle")
			.setAlignHorizontal("center")
			.setFontFamily("Times New Roman")
			.setFontSize(50)
			.setStrokeWidth(2)
			.setFontStyle("italic")
			.setFontVariant("small-caps")
			.setFontWeight(900)
			.setDirection("rtl");
		this.addChild(this.txt);

		this.text2 = new Impacto.GameObjects.Text(100, 100, "Font 2")
			.setAlignVertical("middle")
			.setAlignHorizontal("center");
		this.addChild(this.text2);
	}

	update(delta) {
		this.txt.setText(`Hello World! - ${delta}`);
		this.text2.setAngle(this.text2.angle + 1);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
