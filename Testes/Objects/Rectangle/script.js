console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
// import Impacto from "../../../../dist/Impacto.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		const rect1 = new Impacto.GameObjects.Rectangle(400, 300, 50, 50, "#ff0000");
		this.addChild(rect1);

		this.rect2 = new Impacto.GameObjects.Rectangle(100, 100, 75, 50, "#00ff00")
		// .setOrigin(1)
		// .setOrigin(0.5);
		this.addChild(this.rect2);
	}

	update(delta) {
		this.rect2.setAngle(this.rect2.angle + 1);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
