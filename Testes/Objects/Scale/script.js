console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		// this.obj = new Impacto.GameObjects.Text(400, 300, "Hello World")
		// this.obj = new Impacto.GameObjects.Triangle(400, 300, 80, 60, "#00ff00")
		this.obj = new Impacto.GameObjects.Rectangle(400, 300, 80, 60, "#00ff00")
			// this.obj = new Impacto.GameObjects.Circle(400, 300, 80, "#00ff00")
			// .setOrigin(1)
			.setOrigin(0.5)
		this.addChild(this.obj);
	}

	update(delta) {
		const step = 0.002;
		this.obj.setScale(this.obj.scale.x + step, this.obj.scale.y + step);
	}

	posRender(ctx) {
		// Draw lines
		ctx.strokeStyle = "#ff0000";
		ctx.beginPath();
		ctx.moveTo(0, 0);
		ctx.lineTo(800, 600);
		ctx.stroke();
		ctx.closePath();

		ctx.strokeStyle = "#0000ff";
		ctx.beginPath();
		ctx.moveTo(800, 0);
		ctx.lineTo(0, 600);
		ctx.stroke();
		ctx.closePath();
	}

}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
