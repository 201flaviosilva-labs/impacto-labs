console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		const line = new Impacto.GameObjects.Line(400, 300, 700, 550, "#00ff00")
			.setWidth(10)
			.setHeight(100)
			.setEndX(100)
			.setEndY(-100);
		this.addChild(line);

		console.log(line.getTop());
		console.log(line.getBottom());
		console.log(line.getLeft());
		console.log(line.getRight());
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
