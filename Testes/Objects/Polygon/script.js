console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		// const vertices = [
		// 	{ x: 0, y: 0 },
		// 	{ x: 100, y: 0 },
		// 	{ x: 100, y: 100 },
		// 	{ x: 0, y: 100 },
		// 	// { x: 0, y: 0 },
		// ];
		const vertices = [
			{ x: 0, y: 100 },
			{ x: 10, y: 50 },
			{ x: 90, y: 20 },
			{ x: 20, y: 25 },
			{ x: 30, y: 50 },
			{ x: 25, y: 60 },
			{ x: 0, y: 20 },
			{ x: 0, y: 0 },
		];

		const polygon = new Impacto.GameObjects.Polygon(100, 100, vertices, "#ff0000", "#000000")
			.setClose(true)
			.setStrokeWidth(2)
			.setOrigin(0);
		this.addChild(polygon);

		console.log("Top:", polygon.getTop());
		console.log("Bottom:", polygon.getBottom());
		console.log("Left:", polygon.getLeft());
		console.log("Right:", polygon.getRight());

		console.log("Center:", polygon.getCenter());
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
