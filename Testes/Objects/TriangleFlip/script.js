console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		const triangle1 = new Impacto.GameObjects.Triangle(100, 100, 100, 100, "#ff0000")
		const triangle2 = new Impacto.GameObjects.Triangle(500, 100, 100, 100, "#00ff00").setFlipX(true).setFlipY(true)
		const triangle3 = new Impacto.GameObjects.Triangle(250, 250, 100, 100, "#0000ff").setOrigin(0.5).setFlipX(true).setFlipY(true)
		const triangle4 = new Impacto.GameObjects.Triangle(500, 500, 100, 100, "#ff00ff").setOrigin(1).setFlipX(true).setFlipY(true)
		this.addChild(triangle1);
		this.addChild(triangle2);
		this.addChild(triangle3);
		this.addChild(triangle4);
	}

	update(delta) {
		// this.triangle.setScale(this.triangle.scale.x + 0.001);
	}

	posRender(ctx) {
		for (let i = 0; i < 20; i++) {
			// Left to bottom
			ctx.beginPath();
			ctx.arc(i * 50, i * 50, 2, 0, 2 * Math.PI);
			ctx.fill();
			ctx.stroke();

			// Right to bottom
			ctx.beginPath();
			ctx.arc(600 - i * 50, i * 50, 2, 0, 2 * Math.PI);
			ctx.fill();
			ctx.stroke();
		}
	}
}

const game = new Impacto.Game({
	width: 600,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
