console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		const circle = new Impacto.GameObjects.PhysicsRectangle(400, 300, 50, 50, "#ff0000")
			.setVelocity(10, 10)
			.setCollisionWorldBounds(true)
			.setBounce(1);
		this.addChild(circle);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 1, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
