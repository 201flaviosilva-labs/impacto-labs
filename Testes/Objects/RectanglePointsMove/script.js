console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";
import drawAllPoints from "../../Points.js";

class Game extends Impacto.Scene {
	start() {
		this.rect = new Impacto.GameObjects.PhysicsRectangle(400, 300, 50, 50, "none", "red");
		// this.rect.setOrigin(-1);
		// this.rect.setOrigin(-0.5);
		// this.rect.setOrigin(0);
		this.rect.setOrigin(0.5);
		// this.rect.setOrigin(1);
		this.addChild(this.rect);

		this.rect.setVelocity(10);
		this.rect.setBounce(1);
		this.rect.setCollisionWorldBounds(true);



		console.log("Top -> ", this.rect.getTop());
		console.log("Bottom -> ", this.rect.getBottom());
		console.log("Left -> ", this.rect.getLeft());
		console.log("Right -> ", this.rect.getRight());

		console.log("");

		console.log("Center X -> ", this.rect.getCenterX());
		console.log("Center Y -> ", this.rect.getCenterY());

		console.log("");

		console.log("TopLeft -> ", this.rect.getTopLeft());
		console.log("TopCenter -> ", this.rect.getTopCenter());
		console.log("TopRight -> ", this.rect.getTopRight());

		console.log("");

		console.log("CenterLeft -> ", this.rect.getCenterLeft());
		console.log("Center -> ", this.rect.getCenter());
		console.log("CenterRight -> ", this.rect.getCenterRight());

		console.log("");

		console.log("BottomLeft -> ", this.rect.getBottomLeft());
		console.log("BottomCenter -> ", this.rect.getBottomCenter());
		console.log("BottomRight -> ", this.rect.getBottomRight());
	}

	posRender(ctx) {
		this.drawLines(ctx);
		// Rectangle Points
		drawAllPoints(ctx, this.rect);
	}

	drawLines(ctx) {
		// Draw Lines
		ctx.strokeStyle = "rgba(0, 0, 0, 0.1)"; // Black
		ctx.moveTo(0, 0);
		ctx.lineTo(800, 600);
		ctx.stroke();

		ctx.moveTo(0, 600);
		ctx.lineTo(800, 0);
		ctx.stroke();

		ctx.moveTo(400, 0);
		ctx.lineTo(400, 600);
		ctx.stroke();

		ctx.moveTo(0, 300);
		ctx.lineTo(800, 300);
		ctx.stroke();
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	// gravity: { x: 1, y: 1, },
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
