console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.rect = new Impacto.Rectangle(400, 300, 100, 100, "#ff0000");
		this.rect.setCollisionWorldBounds(true);
		this.rect.setBounce(1);
		this.rect.setVelocity(5, -5);
		this.addChild(this.rect);
		this.collisions.layer1.push(this.rect);

		this.circle = new Impacto.Circle(400, 300, 50, "#00ff00");
		this.circle.setCollisionWorldBounds(true);
		this.circle.setBounce(1);
		this.circle.setVelocity(-5, 5);
		this.addChild(this.circle);
		// this.collisions.layer1.push(this.circle);
	}

	update(delta, fps) {
		console.log(parseInt(fps));
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
