console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		const myText = new Impacto.GameObjects.Text(400, 300, "Paused: false");
		this.addChild(myText);

		setInterval(() => {
			Impacto.State.Global.setPause(!Impacto.State.Global.isPaused);
			console.log(Impacto.State.Global.isPaused);
			myText.setText("Paused: " + Impacto.State.Global.isPaused);
		}, 1000);

		myText.x = 0;
		myText.y = 0;
	}

	update(delta, fps) {
		console.log(parseInt(fps));
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
