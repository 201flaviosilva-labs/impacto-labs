export function drawTopLeft(ctx, rect) {
	ctx.fillStyle = "#ff0000"; // Red
	ctx.beginPath();
	ctx.arc(rect.getTopLeft().x, rect.getTopLeft().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawTopCenter(ctx, rect) {
	ctx.fillStyle = "#00ff00"; // Green
	ctx.beginPath();
	ctx.arc(rect.getTopCenter().x, rect.getTopCenter().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawTopRight(ctx, rect) {
	ctx.fillStyle = "#0000ff"; // Blue
	ctx.beginPath();
	ctx.arc(rect.getTopRight().x, rect.getTopRight().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawCenterLeft(ctx, rect) {
	ctx.fillStyle = "#ff00ff"; // Purple
	ctx.beginPath();
	ctx.arc(rect.getCenterLeft().x, rect.getCenterLeft().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawCenter(ctx, rect) {
	ctx.fillStyle = "#00ffff"; // White Blue
	ctx.beginPath();
	ctx.arc(rect.getCenter().x, rect.getCenter().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawCenterRight(ctx, rect) {
	ctx.fillStyle = "#ffff00"; // Orange
	ctx.beginPath();
	ctx.arc(rect.getCenterRight().x, rect.getCenterRight().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawBottomLeft(ctx, rect) {
	ctx.fillStyle = "#ffffff"; // White
	ctx.beginPath();
	ctx.arc(rect.getBottomLeft().x, rect.getBottomLeft().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawBottomCenter(ctx, rect) {
	ctx.fillStyle = "#000000"; // Black
	ctx.beginPath();
	ctx.arc(rect.getBottomCenter().x, rect.getBottomCenter().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function drawBottomRight(ctx, rect) {
	ctx.fillStyle = "#808080"; // Gray
	ctx.beginPath();
	ctx.arc(rect.getBottomRight().x, rect.getBottomRight().y, 5, 0, Math.PI * 2);
	ctx.fill();
}

export function getRealTopLeft(ctx, rect) {
	ctx.fillStyle = "#ff0000"; // Red
	ctx.beginPath();
	ctx.arc(rect.getRealLeft(), rect.getRealTop(), 5, 0, Math.PI * 2);
	ctx.fill();
}

export function getRealTopRight(ctx, rect) {
	ctx.fillStyle = "#0000ff"; // Blue
	ctx.beginPath();
	ctx.arc(rect.getRealRight(), rect.getRealTop(), 5, 0, Math.PI * 2);
	ctx.fill();
}

export function getRealCenter(ctx, rect) {
	ctx.fillStyle = "#00ffff"; // White Blue
	ctx.beginPath();
	ctx.arc(rect.getRealCenterX(), rect.getRealCenterY(), 5, 0, Math.PI * 2);
	ctx.fill();
}

export function getRealBottomLeft(ctx, rect) {
	ctx.fillStyle = "#ffffff"; //  White
	ctx.beginPath();
	ctx.arc(rect.getRealLeft(), rect.getRealBottom(), 5, 0, Math.PI * 2);
	ctx.fill();
}

export function getRealBottomRight(ctx, rect) {
	ctx.fillStyle = "#808080"; // Gray
	ctx.beginPath();
	ctx.arc(rect.getRealRight(), rect.getRealBottom(), 5, 0, Math.PI * 2);
	ctx.fill();
}


export default function drawAllPoints(ctx, rect) {
	// Points Based in the origin
	drawTopLeft(ctx, rect);
	drawTopCenter(ctx, rect);
	drawTopRight(ctx, rect);
	drawCenterLeft(ctx, rect);
	drawCenter(ctx, rect);
	drawCenterRight(ctx, rect);
	drawBottomLeft(ctx, rect);
	drawBottomCenter(ctx, rect);
	drawBottomRight(ctx, rect);

	// Points Based in the real position
	// getRealTopLeft(ctx, rect);
	// getRealTopRight(ctx, rect);
	// getRealCenter(ctx, rect);
	// getRealBottomLeft(ctx, rect);
	// getRealBottomRight(ctx, rect);
}
