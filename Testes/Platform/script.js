console.clear();
import Impacto from "../../Build/Impacto.esm.js";

class Game extends Impacto.Scene {
	start() {
		this.platform = new Impacto.Rectangle(200, 550, 400, 25, "#000000"); // Static
		this.platform.setStaticBody();
		this.addChild(this.platform);
		this.collisions.layer1.push(this.platform);

		this.player = new Impacto.Rectangle(375, 275, 50, 50, "#ff0000");
		this.player.setCollisionWorldBounds(true);
		this.addChild(this.player);
		this.collisions.layer1.push(this.player);
	}

	update() {
		// Platform Game
		const speed = 10;
		const { left, up, right, down } = Impacto.Inputs.KeyBoard.keys;

		// Horizontal
		if (Impacto.Inputs.KeyBoard.isKeyPressed(left) || Impacto.Inputs.KeyBoard.isKeyPressed("a")) this.player.setVelocityX(-speed);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(right) || Impacto.Inputs.KeyBoard.isKeyPressed("d")) this.player.setVelocityX(speed);
		else this.player.setVelocityX(0);

		// Vertical
		if (Impacto.Inputs.KeyBoard.isKeyPressed(up) || Impacto.Inputs.KeyBoard.isKeyPressed("w")) this.player.setVelocityY(-speed);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(down) || Impacto.Inputs.KeyBoard.isKeyPressed("s")) this.player.setVelocityY(speed);
		else this.player.setVelocityY(0);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 5, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
