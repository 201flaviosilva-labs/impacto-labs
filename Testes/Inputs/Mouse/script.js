console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.rectangle = new Impacto.GameObjects.Rectangle(400, 300, 50, 50, "#00ff00");
		this.rectangle.setOrigin(0.5);
		this.addChild(this.rectangle);
	}

	update(delta, fps) {
		// Mouse
		// console.log(Impacto.Inputs.Mouse.isButtonDown(2));
		// console.log(Impacto.Inputs.Mouse.getNameByButtonCode(1));
		// console.log(Impacto.Inputs.Mouse.getButtonKeyByName("right"));
		// console.log(Impacto.Inputs.Mouse.isButtonDownByName("left"));
		// console.log(Impacto.Inputs.Mouse.isButtonDownByButtonCode(2));
		this.rectangle.setPosition(Impacto.Inputs.Mouse.getPosition().x, Impacto.Inputs.Mouse.getPosition().y);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
