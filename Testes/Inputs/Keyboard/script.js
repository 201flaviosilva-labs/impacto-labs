console.clear();
// import Impacto from "../../../Build/Impacto.esm.js";
import Impacto from "../../../../src/Impacto.js";

class Game extends Impacto.Scene {
	start() {
		this.speed = 10;
		this.rect = new Impacto.GameObjects.Circle(400, 300, 50, "#ff0000")
		// .setOrigin(0.5);
		this.addChild(this.rect);
	}

	update(delta, fps) {
		const { left, up, right, down } = Impacto.Inputs.KeyBoard.keys;

		if (Impacto.Inputs.KeyBoard.isKeyPressed(left) || Impacto.Inputs.KeyBoard.isKeyPressed("a"))
			this.rect.setX(this.rect.x - this.speed * delta);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(right) || Impacto.Inputs.KeyBoard.isKeyPressed("d"))
			this.rect.setX(this.rect.x + this.speed * delta);

		if (Impacto.Inputs.KeyBoard.isKeyPressed(up) || Impacto.Inputs.KeyBoard.isKeyPressed("w"))
			this.rect.setY(this.rect.y - this.speed * delta);
		else if (Impacto.Inputs.KeyBoard.isKeyPressed(down) || Impacto.Inputs.KeyBoard.isKeyPressed("s"))
			this.rect.setY(this.rect.y + this.speed * delta);
	}
}

const game = new Impacto.Game({
	width: 800,
	height: 600,
	parent: "GameContainer",
	gravity: { x: 0, y: 0, },
	backgroundColor: "#f0f0f0",
	debug: true,
	scene: Game,
});
