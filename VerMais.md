## Fisicas e Collisões
- https://github.com/ThaisRobba/ox
- https://github.com/xem/mini2Dphysics/
- https://github.com/KilledByAPixel/LittleJS
- https://js13kgames.github.io/resources/
- (P5 Collide2D (Overlap)) https://github.com/bmoren/p5.collide2D

### Artigos e Forums sobre física
- https://spicyyoghurt.com/tutorials/html5-javascript-game-development/collision-detection-physics
- https://riptutorial.com/html5-canvas/example/17705/are-2-circles-colliding-
- https://developer.ibm.com/tutorials/wa-build2dphysicsengine/
- https://stackoverflow.com/questions/70712165/2d-physics-engine-resolve-collision
- https://stackoverflow.com/questions/45910915/circle-to-circle-collision-response-not-working-as-expected
- https://gamedevelopment.tutsplus.com/tutorials/how-to-create-a-custom-2d-physics-engine-the-basics-and-impulse-resolution--gamedev-6331
- https://ericleong.me/research/circle-circle/#dynamic-circle-circle-collision
- https://stackoverflow.com/questions/54483073/simple-way-to-resolve-2d-elastic-collision-between-circles
- https://codepen.io/Full_of_Symmetries/pen/qqazdW?editors=0010
- https://www.youtube.com/playlist?list=PLo6lBZn6hgca1T7cNZXpiq4q395ljbEI_
- https://js13kgames.github.io/resources/
- https://stackoverflow.com/questions/53486422/ball-to-ball-collision-resolution


## Exemplo de GitHub CDN:
- https://201flaviosilva.github.io/Impacto/dist/Impacto.js

## Boas Docs
- (Exemplos de Docs) https://www.quora.com/What-are-some-examples-of-very-well-made-GitHub-wiki-pages-for-open-source-projects

## Community
- [Web Site](https://201flaviosilva.github.io/Impacto/);
- [Repo](https://github.com/201flaviosilva/Impacto);
- [Wiki/Doc](https://github.com/201flaviosilva/Impacto/wiki);
- [GitHub Forum](https://github.com/201flaviosilva/Impacto/discussions);

- Reddit? -> https://www.reddit.com
- Google Groups? -> https://groups.google.com
- Discord? -> https://discord.gg
