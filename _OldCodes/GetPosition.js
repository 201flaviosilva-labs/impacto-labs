getCenter() { return { x: this.getCenterX(), y: this.getCenterY() }; }

getTopLeft() { return { x: this.getLeft(), y: this.getTop() }; }
getTopCenter() { return { x: this.getCenterX(), y: this.getTop() }; }
getTopRight() { return { x: this.getRight(), y: this.getTop() }; }

getBottomLeft() { return { x: this.getLeft(), y: this.getBottom() }; }
getBottomCenter() { return { x: this.getCenterX(), y: this.getBottom() }; }
getBottomRight() { return { x: this.getRight(), y: this.getBottom() }; }

getCenterLeft() { return { x: this.getLeft(), y: this.getCenterY() }; }
getCenterRight() { return { x: this.getRight(), y: this.getCenterY() }; }
