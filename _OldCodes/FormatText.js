this.formattedText = this.text;
this.textCase = "normal"; // normal, capitalize, lowercase, uppercase, camelCase, snakeCase, kebabCase, pascalCase, sentenceCase

const opt = {
	text: {
		case: {
			normal: "normal",
			capitalize: "capitalize",
			lowercase: "lowercase",
			uppercase: "uppercase",
			camelCase: "camelCase",
			snakeCase: "snakeCase",
			kebabCase: "kebabCase",
			pascalCase: "pascalCase",
			sentenceCase: "sentenceCase",
		},
	},
};

setTextCase(format) {
	this.textCase = format;
	this.formattedText = this.text;
	return this;
}

_updateText() {
	switch (this.textCase) {
		case "capitalize":
			this.formattedText = this.formattedText.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
			break;

		case "lowercase":
			this.formattedText = this.formattedText.toLowerCase();
			break;

		case "uppercase":
			this.formattedText = this.formattedText.toUpperCase();
			break;

		case "camelCase":
			this.formattedText = this.formattedText.replace(/\s(.)/g, (match, group1) => group1.toUpperCase());
			break;

		case "snakeCase":
			this.formattedText = this.formattedText.replace(/\s/g, "_");
			break;

		case "kebabCase":
			this.formattedText = this.formattedText.replace(/\s/g, "-");
			break;

		case "pascalCase":
			this.formattedText = this.formattedText.replace(/\s(.)/g, (match, group1) => group1.toUpperCase());
			this.formattedText = this.formattedText.charAt(0).toUpperCase() + this.formattedText.substr(1);
			break;

		case "sentenceCase":
			this.formattedText = this.text.replace(/\s(.)/g, (match, group1) => group1.toUpperCase());
			break;

		default: // normal
			this.formattedText = this.text;
			break;
	}
}
