import { GlobalStateManagerInstance } from "../State/GlobalStateManager.js";
import { UtilsMathInstance } from "./Math.js";
import Vector2 from "./Vector2.js";

export default class CollisionDetection {
	constructor() { }

	overlapRectangleAndRectangle(rect1, rect2) {
		return (
			rect1.x < rect2.x + rect2.width &&
			rect1.x + rect1.width > rect2.x &&
			rect1.y < rect2.y + rect2.height &&
			rect1.y + rect1.height > rect2.y
		);
	}

	overlapRectangleAndCircle(rect, circle) {
		let dx = Math.abs(circle.x - (rect.x + rect.width / 2));
		let dy = Math.abs(circle.y - (rect.y + rect.height / 2));

		if (dx > circle.radius + rect.width / 2) return false;
		if (dy > circle.radius + rect.height / 2) return false;

		if (dx <= rect.width) return true;
		if (dy <= rect.height) return true;

		dx = dx - rect.width;
		dy = dy - rect.height;

		return (dx * dx + dy * dy <= circle.radius * circle.radius);
	}

	overlapCircleAndCircle(circle1, circle2) {
		const dx = circle1.x - circle2.x;
		const dy = circle1.y - circle2.y;
		const distance = Math.sqrt(dx * dx + dy * dy);
		return (distance < circle1.radius + circle2.radius);
	}

	getAreaOfTwoOverlappingRectangles(rect1, rect2) { // getIntersactionAreaOfTwoOverlapingRectangles
		if (this.overlapRectangleAndRectangle(rect1, rect2)) {
			const xOverlap = Math.min(rect1.x + rect1.width, rect2.x + rect2.width) - Math.max(rect1.x, rect2.x);
			const yOverlap = Math.min(rect1.y + rect1.height, rect2.y + rect2.height) - Math.max(rect1.y, rect2.y);
			return xOverlap * yOverlap;
		}
	}

	getPerimeterOfTwoOverlappingRectangles(rect1, rect2) {
		if (this.overlapRectangleAndRectangle(rect1, rect2)) {
			const xOverlap = Math.min(rect1.x + rect1.width, rect2.x + rect2.width) - Math.max(rect1.x, rect2.x);
			const yOverlap = Math.min(rect1.y + rect1.height, rect2.y + rect2.height) - Math.max(rect1.y, rect2.y);
			return xOverlap + yOverlap;
		}
	}

	getBoundsOfTwoOverlappingRectangles(rect1, rect2) {
		if (this.overlapRectangleAndRectangle(rect1, rect2)) {
			const xOverlap = Math.min(rect1.x + rect1.width, rect2.x + rect2.width) - Math.max(rect1.x, rect2.x);
			const yOverlap = Math.min(rect1.y + rect1.height, rect2.y + rect2.height) - Math.max(rect1.y, rect2.y);
			return {
				x: Math.max(rect1.x, rect2.x),
				y: Math.max(rect1.y, rect2.y),
				width: xOverlap,
				height: yOverlap
			};
		}
	}

	detectRectangleAndRectangleCollisionDirections(rect1, rect2) {
		if (this.overlapRectangleAndRectangle(rect1, rect2)) {
			const bounds = this.getBoundsOfTwoOverlappingRectangles(rect1, rect2);
			// console.log("Collision");
			const merged = new Vector2(rect1.getCenterX() - UtilsMathInstance.rectangleCenterXFromRectangle(bounds),
				rect1.getCenterY() - UtilsMathInstance.rectangleCenterYFromRectangle(bounds));
			// console.log(merged);
			const mergedN = new Vector2(merged.x, merged.y);
			mergedN.normalize();
			const collisions = [];
			// console.log(mergedN);
			if (mergedN.y > 0.5) collisions.push("TOP");
			if (mergedN.y < -0.5) collisions.push("BOTTOM");
			if (mergedN.x < -0.5) collisions.push("LEFT");
			if (mergedN.x > 0.5) collisions.push("RIGHT");
			return collisions;
		}
		return false;
	}


	collisionLayer(layer, scene) {
		layer.forEach((gameObject1, index1) => {
			layer.forEach((gameObject2, index2) => {
				if (index1 >= index2) return;
				this.collisionResponseRectangleAndRectangle(gameObject1, gameObject2);
			});
		});
	}

	collisionResponseRectangleAndRectangle(rect1, rect2) {
		if (!this.overlapRectangleAndRectangle(rect1, rect2)) return;

		if (rect1.bodyType === "D" && rect2.bodyType === "D") { // Dynamic vs Dynamic
			this.collisionResponseDynamicRectangleAndDynamicRectangle(rect1, rect2);

		} else if ((rect1.bodyType === "K" && rect2.bodyType === "D") ||
			(rect1.bodyType === "D" && rect2.bodyType === "K")) { // Dynamic vs Kinetic
			console.log("Dynamic vs Kinetic");

		} else if ((rect1.bodyType === "S" && rect2.bodyType === "D") ||
			(rect1.bodyType === "D" && rect2.bodyType === "S")) { // Dynamic vs Static
			console.log("Dynamic vs Static");
		}
	}

	collisionResponseDynamicRectangleAndDynamicRectangle(rect1, rect2) {
		const vCollision = { x: rect2.x - rect1.x, y: rect2.y - rect1.y };
		const distance = Math.sqrt((rect2.x - rect1.x) * (rect2.x - rect1.x) + (rect2.y - rect1.y) * (rect2.y - rect1.y));
		const vCollisionNorm = { x: vCollision.x / distance, y: vCollision.y / distance };
		const vRelativeVelocity = { x: rect1.velocity.x - rect2.velocity.x, y: rect1.velocity.y - rect2.velocity.y };
		const speed = vRelativeVelocity.x * vCollisionNorm.x + vRelativeVelocity.y * vCollisionNorm.y;

		if (speed < 0) return;

		const impulse = 2 * speed / (1 + 1);
		if (rect1.getBodyType() === "D") {
			rect1.velocity.x -= (impulse * 1 * vCollisionNorm.x);
			rect1.velocity.y -= (impulse * 1 * vCollisionNorm.y);
		}
		if (rect2.getBodyType() === "D") {
			rect2.velocity.x += (impulse * 1 * vCollisionNorm.x);
			rect2.velocity.y += (impulse * 1 * vCollisionNorm.y);
		}
	}

	// resolveCollision(gameObject1, gameObject2) {
	// 	// console.log(gameObject1.checkWillCollideWith(gameObject2));
	// 	// console.log(gameObject1.width, gameObject2.width);

	// 	// if (this.overlapRectangleAndRectangle(gameObject1, gameObject2)) {
	// 	// 	const collisionBoundsRect = this.getBoundsOfTwoOverlappingRectangles(gameObject1, gameObject2);
	// 	// 	const collisionRect = new Rectangle(
	// 	// 		collisionBoundsRect.x,
	// 	// 		collisionBoundsRect.y,
	// 	// 		collisionBoundsRect.width,
	// 	// 		collisionBoundsRect.height,
	// 	// 		"#000000");
	// 	// 	currentScene.addChild(collisionRect);
	// 	// 	// gameObject1.setVelocity(0);
	// 	// 	// gameObject2.setVelocity(0);
	// 	// }
	// 	// // return;

	// 	const collisions = this.detectRectangleAndRectangleCollisionDirections(gameObject1, gameObject2);
	// 	if (collisions.length < 0 || !collisions) return;

	// 	const lastGameObject1XVelocity = gameObject1.velocity.x;
	// 	const lastGameObject1YVelocity = gameObject1.velocity.y;
	// 	const lastGameObject2XVelocity = gameObject2.velocity.x;
	// 	const lastGameObject2YVelocity = gameObject2.velocity.y;
	// 	const gravityX = GlobalStateManagerInstance.gravity.x;
	// 	const gravityY = GlobalStateManagerInstance.gravity.y;

	// 	// Vertical
	// 	if (collisions.includes("TOP") || collisions.includes("BOTTOM")) {
	// 		// console.log("TOP/BOTTOM");
	// 		if (gameObject1.bodyType === "D" && gameObject2.bodyType === "D") { // Dynamic X Dynamic
	// 			gameObject1.setVelocityY(
	// 				lastGameObject2YVelocity * gameObject1.bounce.y - gravityY
	// 			);
	// 			gameObject2.setVelocityY(
	// 				lastGameObject1YVelocity * gameObject2.bounce.y - gravityY
	// 			);

	// 		} else if (gameObject1.bodyType !== "D" && gameObject2.bodyType === "D") { // (Kinematic || Static) X Dynamic
	// 			gameObject2.setVelocityY(
	// 				((lastGameObject2YVelocity * -1) + lastGameObject1YVelocity) * gameObject2.bounce.y - gravityY
	// 			);

	// 		} else if (gameObject1.bodyType === "D" && gameObject2.bodyType !== "D") { // Dynamic X (Kinematic || Static)
	// 			gameObject1.setVelocityY(
	// 				((lastGameObject1YVelocity * -1) + lastGameObject2YVelocity) * gameObject1.bounce.y - gravityY
	// 			);
	// 		}
	// 	}

	// 	// Horizontal
	// 	if (collisions.includes("LEFT") || collisions.includes("RIGHT")) {
	// 		// console.log("LEFT/RIGHT");
	// 		if (gameObject1.bodyType === "D" && gameObject2.bodyType === "D") { // Dynamic X Dynamic
	// 			gameObject1.setVelocityX(
	// 				lastGameObject2XVelocity * gameObject1.bounce.x - gravityX
	// 			);
	// 			gameObject2.setVelocityX(
	// 				lastGameObject1XVelocity * gameObject2.bounce.x - gravityX
	// 			);

	// 		} else if (gameObject1.bodyType !== "D" && gameObject2.bodyType === "D") { // (Kinematic || Static) X Dynamic
	// 			gameObject2.setVelocityX(
	// 				((lastGameObject2XVelocity * -1) + lastGameObject1XVelocity) * gameObject2.bounce.x - gravityX
	// 			);

	// 		} else if (gameObject1.bodyType === "D" && gameObject2.bodyType !== "D") { // Dynamic X (Kinematic || Static)
	// 			gameObject1.setVelocityX(
	// 				((lastGameObject1XVelocity * -1) + lastGameObject2XVelocity) * gameObject1.bounce.x - gravityX
	// 			);
	// 		}
	// 	}
	// }
}

export const CollisionDetectionInstance = new CollisionDetection();
