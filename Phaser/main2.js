// -- Imports
import Assets from "./Assets.js";

// -- Configs
const FPSDiv = document.getElementById("FPS");
const world = {
	width: 800,
	height: 600,
	middleWidth: 0,
	middleHeight: 0,
};
world.middleWidth = world.width / 2;
world.middleHeight = world.height / 2;

// -- Game
class MainScene extends Phaser.Scene {
	constructor() {
		super({ key: "Home", });
	}

	preload() {
		this.load.image("Azul", Assets.Sprites.Quadrados.Azul);
		this.load.image("Verde", Assets.Sprites.Quadrados.Verde);
		this.load.image("Vermelho", Assets.Sprites.Quadrados.Vermelho);

		this.load.image("Amarelo", Assets.Sprites.Quadrados.Amarelo);
		this.load.image("AzulClaro", Assets.Sprites.Quadrados.AzulClaro);
		this.load.image("Rosa", Assets.Sprites.Quadrados.Rosa);
	}

	create() {
		const { width, height, middleWidth, middleHeight } = world;

		const horizontal1 = this.physics.add.sprite(100, 250, "Vermelho").setVelocityY(100).setBounce(1).setCollideWorldBounds(true).setScale(1.5);

		const vertical1 = this.physics.add.staticImage(300, 550, "Amarelo").setScale(100, 1.5).refreshBody();

		this.physics.add.collider(horizontal1, vertical1);
	}

	update(time, delta) {
		FPSDiv.innerHTML = game.loop.actualFps.toFixed(1);
	}
}

const config = {
	width: world.width,
	height: world.height,
	type: Phaser.Auto,
	backgroundColor: "#000000",
	physics: {
		default: "arcade",
		arcade: {
			debug: true,
		}
	},
	scene: [MainScene],
};
const game = new Phaser.Game(config);
