// -- Imports
import Assets from "./Assets.js";

// -- Configs
const FPSDiv = document.getElementById("FPS");
const world = {
	width: 800,
	height: 600,
	middleWidth: 0,
	middleHeight: 0,
};
world.middleWidth = world.width / 2;
world.middleHeight = world.height / 2;

// -- Game
class MainScene extends Phaser.Scene {
	constructor() {
		super({ key: "Home", });
	}

	preload() {
		this.load.image("Azul", Assets.Sprites.Quadrados.Azul);
		this.load.image("Verde", Assets.Sprites.Quadrados.Verde);
		this.load.image("Vermelho", Assets.Sprites.Quadrados.Vermelho);

		this.load.image("Amarelo", Assets.Sprites.Quadrados.Amarelo);
		this.load.image("AzulClaro", Assets.Sprites.Quadrados.AzulClaro);
		this.load.image("Rosa", Assets.Sprites.Quadrados.Rosa);
	}

	create() {
		const { width, height, middleWidth, middleHeight } = world;

		const horizontal1 = this.physics.add.sprite(550, 250, "Vermelho")
			.setVelocityX(100)
			.setBounce(1)
			.setCollideWorldBounds(true)
			.setScale(1.5)
			.setImmovable(false);

		const horizontal2 = this.physics.add.sprite(700, 250, "Verde")
			.setVelocityX(-100)
			.setBounce(1)
			.setCollideWorldBounds(true)
			.setScale(1.5)
			.setImmovable(true);

		this.physics.add.collider(horizontal1, horizontal2);
	}

	update(time, delta) {
		FPSDiv.innerHTML = game.loop.actualFps;
	}
}

const config = {
	width: world.width,
	height: world.height,
	type: Phaser.Auto,
	backgroundColor: "#000000",
	physics: {
		default: "arcade",
		arcade: {
			debug: true,
		}
	},
	scene: [MainScene],
};
const game = new Phaser.Game(config);
