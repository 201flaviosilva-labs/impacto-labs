// -- Imports
import Assets from "./Assets.js";

// -- Configs
const FPSDiv = document.getElementById("FPS");
const world = {
	width: 800,
	height: 600,
	middleWidth: 0,
	middleHeight: 0,
};
world.middleWidth = world.width / 2;
world.middleHeight = world.height / 2;

// -- Game
class MainScene extends Phaser.Scene {
	constructor() {
		super({ key: "Home", });
	}

	preload() {
		this.load.image("Azul", Assets.Sprites.Bola.Azul);
		this.load.image("Verde", Assets.Sprites.Bola.Verde);
		this.load.image("Vermelho", Assets.Sprites.Bola.Vermelha);

		this.load.image("Amarelo", Assets.Sprites.Bola.Amarelo);
		this.load.image("AzulClaro", Assets.Sprites.Bola.AzulClaro);
		this.load.image("Rosa", Assets.Sprites.Bola.Rosa);
	}

	create() {
		const { width, height, middleWidth, middleHeight } = world;

		const isHorizontal = true;
		const isVertical = true;

		const boxHorizontal = [];
		const boxVertical = [];

		if (isHorizontal) {
			const horizontal1 = this.physics.add.sprite(100, 250, "Vermelho")
				.setVelocityX(25)
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setCircle(16);

			const horizontal2 = this.physics.add.sprite(600, 250, "Azul")
				.setVelocityX(-50)
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setCircle(16);

			const horizontal3 = this.physics.add.sprite(700, 250, "Verde")
				.setVelocityX(100)
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setCircle(16);

			boxHorizontal.push(horizontal1, horizontal2, horizontal3);
			this.physics.add.collider(boxHorizontal);
		}

		if (isVertical) {
			const vertical1 = this.physics.add.sprite(300, 100, "Amarelo")
				.setVelocityY(25)
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setCircle(16);

			const vertical2 = this.physics.add.sprite(310, 400, "AzulClaro")
				.setVelocityY(-100)
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setCircle(16);

			const vertical3 = this.physics.add.sprite(300, 500, "Rosa")
				.setVelocityY(50)
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setCircle(16);

			boxVertical.push(vertical1, vertical2, vertical3);
			this.physics.add.collider(boxVertical);
		}

		this.physics.add.collider(boxHorizontal, boxVertical);
	}

	update(time, delta) {
		FPSDiv.innerHTML = game.loop.actualFps.toFixed(1);
	}
}

const config = {
	width: world.width,
	height: world.height,
	type: Phaser.Auto,
	backgroundColor: "#000000",
	physics: {
		default: "arcade",
		arcade: {
			debug: true,
		}
	},
	scene: [MainScene],
};
const game = new Phaser.Game(config);
