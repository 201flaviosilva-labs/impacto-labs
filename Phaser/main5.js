// -- Imports
import Assets from "./Assets.js";

function randomInt(min, max) { return Math.floor(Math.random() * (max - min) + min); };

// -- Configs
const FPSDiv = document.getElementById("FPS");
const world = {
	width: 800,
	height: 600,
	middleWidth: 0,
	middleHeight: 0,
};
world.middleWidth = world.width / 2;
world.middleHeight = world.height / 2;

// -- Game
class MainScene extends Phaser.Scene {
	constructor() {
		super({ key: "Home", });
	}

	preload() {
		this.load.image("Azul", Assets.Sprites.Quadrados.Azul);
		this.load.image("Verde", Assets.Sprites.Quadrados.Verde);
		this.load.image("Vermelho", Assets.Sprites.Quadrados.Vermelho);

		this.load.image("Amarelo", Assets.Sprites.Quadrados.Amarelo);
		this.load.image("AzulClaro", Assets.Sprites.Quadrados.AzulClaro);
		this.load.image("Rosa", Assets.Sprites.Quadrados.Rosa);
	}

	create() {
		const { width, height, middleWidth, middleHeight } = world;

		const rects = [];

		for (let i = 0; i < 10; i++) {
			const rect = this.physics.add.sprite(550, 250, "Vermelho")
				.setVelocity(randomInt(-100, 100), randomInt(-100, 100))
				.setBounce(1)
				.setCollideWorldBounds(true)
				.setScale(1.5)
				.setRandomPosition();
			rects.push(rect);
		}


		this.physics.add.collider(rects);
	}

	update(time, delta) {
		FPSDiv.innerHTML = game.loop.actualFps;
	}
}

const config = {
	width: world.width,
	height: world.height,
	type: Phaser.Auto,
	backgroundColor: "#000000",
	physics: {
		default: "arcade",
		arcade: {
			debug: true,
		}
	},
	scene: [MainScene],
};
const game = new Phaser.Game(config);
